"""
Des: 
    - this script will be triggered by state machine
    - this script is to refund the investment amount from robo wallet to user wallet
    - state machine will pass the output from this script to the next lambda funcion 

Dependency: 
    - dynamodb -> { prod / stage / dev }_{ demo / live }_user_wallet
    - dynamodb -> { prod / stage / dev }_{ demo / live }_robo_wallet
    - environment variables of lambda:
        os.environ["user_table"] = "user_wallet"
        os.environ["robo_table"] = "robo_wallet"
        os.environ["env"] = "dev" # dev / stage / prod
      
Sample Input:
    # event passed from step function
    {
          'statusCode': 203, 
          'body': '{
              "event": {
                  "user_id": "001", 
                  "action": "deposit", 
                  "currency": "USD", 
                  "status": "pending reverse", 
                  "received_at": "2020-10-19 08:59:10.881638", 
                  "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e", 
                  "amount": "100", 
                  "account_type": "demo", 
                  "robo_type": "Robo_USD",
                  "error_msg": "Fail to save to dynamodb and sqs, _id: 'a8098c1a-f86e-11da-bd1a-00112444be1e'"}, 
              "msg": "Successfully updated status, _id: 'a8098c1a-f86e-11da-bd1a-00112444be1e', New status: 'pending reverse'"
          }'
     }
    
Sample Output (passed to next lambda function through step function): 
    # case #1 - refunded
    {
         'statusCode': 200, 
         'body': '{
             "event": {
                         "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e", 
                         "user_id": "001", 
                         "robo_type": "Robo_USD", 
                         "amount": "100", 
                         "action": "deposit", 
                         "currency": "USD", 
                         "account_type": "demo", 
                         "received_at": "2020-10-19 08:59:10.881638", 
                         "status": "pending reverse",
                         "error_msg": "Fail to save to dynamodb and sqs, _id: 'a8098c1a-f86e-11da-bd1a-00112444be1e'"}, 
                     }, 
             "new_status": "refunded"
         }'
    }
    
    # case #2 - rejected
    {
         'statusCode': 202, 
         'body': '{
             "event": {
                         "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e", 
                         "user_id": "001", 
                         "robo_type": "Robo_USD", 
                         "amount": "1000000000", 
                         "action": "deposit", 
                         "currency": "USD", 
                         "account_type": "demo", 
                         "received_at": "2020-10-19 08:59:10.881638", 
                         "status": "pending reverse",
                         "error_msg": "Fail to save to dynamodb and sqs, _id: 'a8098c1a-f86e-11da-bd1a-00112444be1e'"}, 
                     }, 
             "new_status": "rejected", 
             "error_msg": "error_msg" : "Fail to update robo and/or user wallet, Robo _id: 'Robo_USD', User _id: 'a8098c1a-f86e-11da-bd1a-00112444be1e'"
         }'
    }
    
Author:
    Tessy
    
Log:
    Tessy   18 Oct 2020     First version of this script
    
Status: 
    Alpha
"""

# import libs
import os
import json
try:
    from PentaipAwsFm.pluster.pluster_core.dynamodb_handler import DataHandler as Dynamodb_DataHandler
except:
    from pluster_core.dynamodb_handler import DataHandler as Dynamodb_DataHandler
  
# log messages
INFO_SF_TRIGGERED = "[INFO] Triggered by step function, File: 'Robo Order Refund'"
INFO_API_TRIGGERED = "[INFO] Triggered by api, File: 'Robo Order Refund'"
INFO_UPDATE_USER_SUCCESS_DYNAMODB = "[INFO] Updated success to dynamodb for user bal, File: 'Robo Order Refund', _id: '{}', Table: '{}_{}', New balance: '{}'"
INFO_UPDATE_ROBO_SUCCESS_DYNAMODB = "[INFO] Updated success to dynamodb for robo bal, File: 'Robo Order Refund', _id: '{}', Table: '{}_{}', New balance: '{}'"

DEBUG_START_LAMBDA = "[DEBUG] Start lambda, File: 'Robo Order Refund'"
DEBUG_GET_USER_BAL = "[DEBUG] Getting user balance, File: 'Robo Order Refund', _id: '{}', Table: '{}_{}'"
DEBUG_GET_ROBO_BAL = "[DEBUG] Getting robo balance, File: 'Robo Order Refund', _id: '{}', Table: '{}_{}'"
DEBUG_UPDATE_USER_DYNAMODB = "[DEBUG] Updating user bal to dynamodb, File: 'Robo Order Refund', _id: '{}', Table: '{}_{}', New balance: '{}'"
DEBUG_UPDATE_ROBO_DYNAMODB = "[DEBUG] Updating robo bal to dynamodb, File: 'Robo Order Refund', _id: '{}', Table: '{}_{}', New balance: '{}'"

WARN_STATUS_CODE = "[WARNING] Invalid status code, File: 'Robo Order Refund', Status code: '{}'"
WARN_GET_EVENT = "[WARNING] Failed to get event, File: 'Robo Order Refund', Due to: '{}'"
WARN_NO_EVENT = "[WARNING] No event data found, File: 'Robo Order Refund'"
WARN_EVENT_NOT_DICT = "[WARNING] Event is not dictionary, File: 'Robo Order Refund', Datatype: '{}'"
WARN_NO_USER_RECORD = "[WARNING] User has no existing wallet record in database, File: 'Robo Order Refund', _id: '{}'"
WARN_NO_ROBO_RECORD = "[WARNING] Robo has no existing wallet record in database, File: 'Robo Order Refund', _id: '{}'"
WARN_TRANSFER_BAL = "[WARNING] Unable to transfer balance from robo to user wallet, File: 'Robo Order Refund', Due to: '{}'"
WARN_BAL_NOT_UPDATED = "[WARNING] Robo/User balance not updated, File: 'Robo Order Refund', Ori robo balance: '{}', Updated robo balance: '{}', Ori user balance: '{}', Updated user balance: '{}'"
WARN_UPDATE_USER_FAIL_DYNAMODB = "[WARNING] Updated fail to dynamodb for user bal, File: 'Robo Order Refund', _id: '{}', Table: '{}_{}', New balance: '{}'"
WARN_UPDATE_ROBO_FAIL_DYNAMODB = "[WARNING] Updated fail to dynamodb for robo bal, File: 'Robo Order Refund', _id: '{}', Table: '{}_{}', New balance: '{}'"

ERROR_UPDATE_USER_DYNAMODB = "[ERROR] Unable to update user bal to dynamodb, File: 'Robo Order Refund', Due to: '{}'"
ERROR_UPDATE_ROBO_DYNAMODB = "[ERROR] Unable to update robo bal to dynamodb, File: 'Robo Order Refund', Due to: '{}'"
   
def __is_sf( event ):
    '''check whether event is triggered by step function
    '''
    # checking 
    if event and event.get("statusCode"):
        # return
        return True
        
    # return
    return False

def __is_valid_status_code( event ):
    '''check status code
    '''
    # checking
    return True if event.get("statusCode") in [ 203 ] else False

def __is_api_gw( event ):
    '''check whether event is triggered by api
    '''
    # checking 
    if event and event.get("httpMethod"):
        # return
        return True
        
    # return
    return False

def get_sf_event( event ):
    '''get event from body of sf
    '''
    # get data
    event = event.get( "body", {} )
    event = json.loads( event )
    
    # return
    return event.get( "event", {} )
    
def get_api_event( event ):
    '''get event from body of api
    '''
    # get data
    event = event.get( "body", {} )
    event = json.loads( event )
    
    # return
    return event
    
def get_robo_balance( event, dynamo_db ):
    '''get robo balance data
    '''
    # define variable
    dbname = os.environ["env"]
    colname = "{}_{}".format( event.get("account_type"), os.environ["robo_table"] )
    
    # log 
    print( DEBUG_GET_ROBO_BAL.format( event.get("robo_type"), dbname, colname ) )

    # get and return data
    return dynamo_db.get( dbname, colname, event.get("robo_type") )

def get_user_balance( event, dynamo_db ):
    '''get user balance data
    '''
    # define variable
    dbname = os.environ["env"]
    colname = "{}_{}".format( event.get("account_type"), os.environ["user_table"] )
    _id = "{}_{}".format( event.get("user_id"), event.get("currency") ) 
    
    # log 
    print( DEBUG_GET_USER_BAL.format( _id, dbname, colname ) )

    # get and return data
    return dynamo_db.get( dbname, colname, _id )

def __is_float( amount ):
    '''check whether the 'amount' value is convertible to float
    '''
    try:
        float( amount )
        
    except ValueError:
        # return
        return "ValueError"

def reverse_user2robo_wallet( event, user_balance, robo_balance ):
    '''transfer balance from robo to user wallet
    '''
    # checking
    if __is_float( event.get("amount") ) != "ValueError" and \
       __is_float( robo_balance.get("wallet_balance") ) != "ValueError" and \
       __is_float( user_balance.get("balance") ) != "ValueError":
           
       # add and subtract balance
       robo_balance["wallet_balance"] = str( float( robo_balance.get("wallet_balance") ) - float( event.get("amount") ) )
       user_balance["balance"] = str( float( user_balance.get("balance") ) + float( event.get("amount") ) )
        
    else:
        raise Exception( "Unable to convert either deposit amount or user wallet bal or robo wallet bal to float" )

    # return
    return robo_balance, user_balance
    
def update_robo_balance( account_type, updated_robo_balance, dynamo_db ):
    '''update robo balance to db
    '''
    # define variable
    dbname = os.environ["env"]
    colname = "{}_{}".format( account_type, os.environ["robo_table"] )
    
    # log 
    print( DEBUG_UPDATE_ROBO_DYNAMODB.format( updated_robo_balance.get("_id"), dbname, colname, updated_robo_balance.get("wallet_balance") ) )
  
    # save
    res = dynamo_db.set( dbname, colname, updated_robo_balance )
    
    # check saving status
    if res["ResponseMetadata"].get("HTTPStatusCode") == 200:
        # log
        print( INFO_UPDATE_ROBO_SUCCESS_DYNAMODB.format( updated_robo_balance.get("_id"), dbname, colname, updated_robo_balance.get("wallet_balance") ) )
        
        # return
        return True
    
    else: 
        # log
        print( WARN_UPDATE_ROBO_FAIL_DYNAMODB.format( updated_robo_balance.get("_id"), dbname, colname, updated_robo_balance.get("wallet_balance") ) )
        
        # return
        return False
    
def update_user_balance( account_type, updated_user_balance, dynamo_db ):
    '''update user balance to db
    '''
    # define variable
    dbname = os.environ["env"]
    colname = "{}_{}".format( account_type, os.environ["user_table"] )
    
    # log 
    print( DEBUG_UPDATE_USER_DYNAMODB.format( updated_user_balance.get("_id"), dbname, colname, updated_user_balance.get("balance") ) )

    # save
    res = dynamo_db.set( dbname, colname, updated_user_balance )
    
    # check saving status
    if res["ResponseMetadata"].get("HTTPStatusCode") == 200:
        # log
        print( INFO_UPDATE_USER_SUCCESS_DYNAMODB.format( updated_user_balance.get("_id"), dbname, colname, updated_user_balance.get("balance") ) )
        
        # return
        return True
    
    else: 
        # log
        print( WARN_UPDATE_USER_FAIL_DYNAMODB.format( updated_user_balance.get("_id"), dbname, colname, updated_user_balance.get("balance") ) )
        
        # return
        return False

def fm_init_deco( func ):
    def inner( event, context ):
        '''initialize
        '''
        # log
        print( DEBUG_START_LAMBDA )
        
        # checking
        if not isinstance( event, dict ):
            # log
            print( WARN_EVENT_NOT_DICT.format( type( event ) ) )
            
            # return
            return {
                        "statusCode": 404,
                        "body": json.dumps( "Event must be dictionary" )
                    }
        
        # return function
        return func( event, context )
    
    # return
    return inner

def fm_security_checking_deco( func ):
    def inner( event, context ):
        '''checking the input for:
        - verification
        '''
        if __is_sf( event ):
            # log
            print( INFO_SF_TRIGGERED )
            
            if not __is_valid_status_code( event ):        
                # log
                print( WARN_STATUS_CODE.format( event.get("statusCode") ) )
                
                # return
                return {
                            "statusCode": 404,
                            "body" : json.dumps( "Invalid status code" )
                        }
            
            else:
                # get event
                try:
                    event = get_sf_event( event )
                    
                except Exception as e:
                    # log
                    print( WARN_GET_EVENT.format( str(e) ) )
                    
                    # return
                    return {
                                "statusCode": 404,
                                "body": json.dumps( "Failed to get sf event, Due to: '{}'".format( str(e) ) )
                            }
           
        elif __is_api_gw( event ):
            # log 
            print( INFO_API_TRIGGERED )
            
            # get event
            try:
                event = get_api_event( event )
                
            except Exception as e:
                # log
                print( WARN_GET_EVENT.format( str(e) ) )
                
                # return
                return {
                            "statusCode": 404,
                            "body": json.dumps( "Failed to get api event, Due to: '{}'".format( str(e) ) )
                        }
        
        # return function
        return func( event, context )
    
    # return
    return inner

def fm_data_checking_deco( func ):
    def inner( event, context ):
        '''checking the input for:
        - invalid data
        '''    
        # checking
        if not event:
            # log
            print( WARN_NO_EVENT )
            
            # return
            return {
                        "statusCode": 204,
                        "body" : json.dumps( "No event data found" )
                    }
    
        if not isinstance( event, dict ):
            # log
            print( WARN_EVENT_NOT_DICT.format( type( event ) ) )
            
            # return
            return {
                        "statusCode": 404,
                        "body" : json.dumps( "Event must be dictionary" )
                    }
        
        # return function
        return func(event, context)
        
    # return
    return inner

@fm_init_deco
@fm_security_checking_deco
@fm_data_checking_deco
def main( event, context ):
    '''main
    sample input
    ------------
    # define event
    event = {
                  "user_id": "001",
                  "action": "deposit",
                  "currency": "USD",
                  "status": "pending reverse",
                  "received_at": "2020-10-19 08:59:10.881638",
                  "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e",
                  "amount": "100",
                  "account_type": "demo",
                  "robo_type": "Robo_USD",
                  "error_msg": "Fail to save to dynamodb and sqs, _id: 'a8098c1a-f86e-11da-bd1a-00112444be1e'"
            }
        
    # sample step function event
    event = {
                    "statusCode": 203,
                    "body": json.dumps( {
                        "event": event,
                        "msg":  "Successfully updated status, _id: '{}', New status: '{}'".format( event.get("_id"), event.get("status") )
                    } )
                }
    
    
    # sample api event (not full version)
    event = {
                "httpMethod" : "POST",
                "body": json.dumps( event )
            }
    '''
    # connect
    dynamo_db = Dynamodb_DataHandler()
    dynamo_db.connect()

    # get robo balance
    robo_balance = get_robo_balance( event, dynamo_db )
    
    # checking
    if not robo_balance:
        # log
        print( WARN_NO_ROBO_RECORD.format( event.get("robo_type") ) )
        
        # close connection
        dynamo_db.close()
        
        # return
        return {
                    "statusCode": 202,
                    "body": json.dumps( {
                        "event": event,
                        "new_status": "rejected",
                        "error_msg" : "Robo has no existing wallet record in database, Robo: '{}'".format( event.get("robo_type") )
                    } )
                }
        
    # get user balance
    user_balance = get_user_balance( event, dynamo_db )
      
    # checking
    if not user_balance:
        # log
        print( WARN_NO_USER_RECORD.format( event.get("user_id") + "_" + event.get("currency") ) )
        
        # close connection
        dynamo_db.close()
        
        # return
        return {
                    "statusCode": 202,
                    "body": json.dumps( {
                        "event": event,
                        "new_status": "rejected",
                        "error_msg" : "User has no existing wallet record in database, User: '{}_{}'".format( event.get("user_id"), event.get("currency") )
                    } )
                }
            
    # define variable
    robo_res = False
    user_res = False
    updated_robo_balance = None
    updated_user_balance = None
    ori_robo_wallet_bal = robo_balance.get("wallet_balance")
    ori_user_wallet_bal = user_balance.get("balance")
    
    # transfer balance
    try:
        updated_robo_balance, updated_user_balance = reverse_user2robo_wallet( event, user_balance, robo_balance )

    except Exception as e:
        # log
        print( WARN_TRANSFER_BAL.format( str(e) ) )
        
        # close connection
        dynamo_db.close()
        
        # return
        return {
                    "statusCode": 202,
                    "body": json.dumps( {
                        "event": event,
                        "new_status": "rejected",
                        "error_msg" : "Unable to transfer balance from robo to user wallet, Due to: '{}'".format( str(e) )
                    } )
                }
        
    # checking
    if not updated_robo_balance or updated_robo_balance.get("wallet_balance") == ori_robo_wallet_bal or \
       not updated_user_balance or updated_user_balance.get("balance") == ori_user_wallet_bal:
        # log
        print( WARN_BAL_NOT_UPDATED.format( ori_robo_wallet_bal, updated_robo_balance.get("wallet_balance"), ori_user_wallet_bal, updated_user_balance.get("balance") ) )
        
        # close connection
        dynamo_db.close()
        
        # return
        return {
                    "statusCode": 202,
                    "body": json.dumps( {
                        "event": event,
                        "new_status": "rejected",
                        "error_msg" : "Robo/User balance not updated"
                    } )
                }
    
    # save updated robo balance
    try:
        robo_res = update_robo_balance( event.get("account_type"), updated_robo_balance, dynamo_db )
    
    except Exception as e:
        # log
        print( ERROR_UPDATE_ROBO_DYNAMODB.format( str(e) ) )
    
    # save updated user balance
    if robo_res:
        try:
            user_res = update_user_balance( event.get("account_type"), updated_user_balance, dynamo_db )
    
        except Exception as e:
            # log
            print( ERROR_UPDATE_USER_DYNAMODB.format( str(e) ) )
        
    # close connection
    dynamo_db.close()
        
    # return
    if user_res:
        return {
                    "statusCode": 200,
                    "body": json.dumps( {
                        "event": event,
                        "new_status": "refunded",
                    } )
                }
        
    else:
        return {
                    "statusCode": 202,
                    "body": json.dumps( {
                        "event": event,
                        "new_status": "rejected",
                        "error_msg" : "Fail to update robo and/or user wallet, Robo _id: '{}', User _id: '{}'".format( updated_robo_balance.get("_id"), updated_user_balance.get("_id") )
                    } )
                }
    
    
# # test run 
# # TODO: comment this before push to aws
# if __name__ == "__main__":
#     # simulate aws environment
#     os.environ["env"] = "dev"
#     os.environ["user_table"] = "user_wallet"
#     os.environ["robo_table"] = "robo_wallet"
    
#     # define variable
#     event = {
#                   "user_id": "001",
#                   "action": "deposit",
#                   "currency": "USD",
#                   "status": "pending reverse",
#                   "received_at": "2020-10-19 08:59:10.881638",
#                   "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e",
#                   "amount": "100",
#                   "account_type": "demo",
#                   "robo_type": "Robo_USD",
#                   "error_msg": "Fail to save to dynamodb and sqs, _id: 'a8098c1a-f86e-11da-bd1a-00112444be1e'"
#             }
        
#     # # sample step function event
#     # event = {
#     #                 "statusCode": 203,
#     #                 "body": json.dumps( {
#     #                     "event": event,
#     #                     "msg":  "Successfully updated status, _id: '{}', New status: '{}'".format( event.get("_id"), event.get("status") )
#     #                 } )
#     #             }
    
    
#     # # sample api event (not full version)
#     # event = {
#     #             "httpMethod" : "POST",
#     #             "body": json.dumps( event )
#     #         }
    
#     res = main( event, "" )