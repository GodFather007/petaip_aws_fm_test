"""
Des: 
    - this script will be triggered by frontend using API
    - this script will save data to dynamodb and sqs
    - add "received_at" and "status" fields before saving to dynamodb and sqs

Dependency: 
    - dynamodb -> { prod / stage / dev }_{ demo / live }_fm_submit_req 
    - sqs -> { prod / stage / dev }_{ demo / live }_fm_submit_req_queue 
    - environment variables of lambda:
        os.environ["table"] = "fm_submit_req"
        os.environ["queue"] = "fm_submit_req"
        os.environ["env"] = "dev" # dev / stage / prod
        
Sample Input:
    # sample input for API
    {
        "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e",
        "user_id": "001",
        "robo_type": "Robo_USD",
        "amount": "100",
        "action": "deposit",
        "currency": "USD",
        "account_type": "demo"
    }
    
    # sample api event passed into this script (not full version)
    event = {
                "httpMethod" : "POST",
                "body": json.dumps( event )
            }
        
Exception:
    - Invalid api event ( return 404 )
    - Missing field ( return 404 )
    - Non-string datatype ( return 404 )
    - Invalid value for 'account_type' / 'action' / 'status' / 'currency' / 'amount' ( return 404 )

Sample Output: 
    {'statusCode': 200, 'body': '"Successfully saved to dynamodb and sqs, _id: \'a8098c1a-f86e-11da-bd1a-00112444be1e\'"'}
    
Sample Data ( data/msg saved to dynamodb/sqs ):
    {
        "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e",
        "user_id": "001",
        "robo_type": "Robo_USD",
        "amount": "100",
        "action": "deposit",
        "currency": "USD",
        "account_type": "demo",
        "status": "received",
        "received_at": "2020-10-09 03:57:44.919435"
    }
    
Author:
    Tessy
    
Log:
    Tessy   05 Oct 2020     First version of this script
    Tessy   19 Oct 2020     Added some checking and made some amendments to the logging
    
Status: 
    Alpha
"""

# import libs
import os
import json
import datetime
try:
    from PentaipAwsFm.pluster.pluster_core.sqs_handler import DataHandler as Sqs_DataHandler
    from PentaipAwsFm.pluster.pluster_core.dynamodb_handler import DataHandler as Dynamodb_DataHandler
except:
    from pluster_core.sqs_handler import DataHandler as Sqs_DataHandler
    from pluster_core.dynamodb_handler import DataHandler as Dynamodb_DataHandler
  
# log messages
INFO_API_TRIGGERED = "[INFO] Triggered by api, File: 'Request Receive'"
INFO_SAVE_SUCCESS_DYNAMODB = "[INFO] Saved success to dynamodb, File: 'Request Receive', _id: '{}', Table: '{}_{}'"
INFO_SAVE_SUCCESS_SQS = "[INFO] Saved success to sqs, File: 'Request Receive', _id: '{}', Queue: '{}_queue'"

DEBUG_START_LAMBDA = "[DEBUG] Start lambda, File: 'Request Receive'"
DEBUG_START_LAMBDA_ID = "[DEBUG] Start lambda by ID, File: 'Request Receive', _id: '{}'"
DEBUG_SAVE_DYNAMODB = "[DEBUG] Saving to dynamodb, File: 'Request Receive', _id: '{}', Table: '{}_{}'"
DEBUG_SAVE_SQS = "[DEBUG] Saving to sqs, File: 'Request Receive', _id: '{}', Queue: '{}_queue'"

WARN_START_LAMBDA_NO_ID = "[WARNING] Start lambda with no ID, File: 'Request Receive', Event: '{}'"
WARN_EVENT_NOT_DICT = "[WARNING] Event is not dictionary, File: 'Request Receive', Datatype: '{}'"
WARN_NO_EVENT = "[WARNING] No event data found, File: 'Request Receive'"
WARN_GET_EVENT = "[WARNING] Failed to get event, File: 'Request Receive', Due to: '{}'"
WARN_INVALID_EVENT = "[WARNING] Event invalid, File: 'Request Receive', Error msg: '{}'"
WARN_SAVE_FAIL_DYNAMODB = "[WARNING] Saved fail to dynamodb, File: 'Request Receive', _id: '{}', Table: '{}_{}'"
WARN_SAVE_FAIL_SQS = "[WARNING] Saved fail to sqs, File: 'Request Receive', _id: '{}', Queue: '{}_queue'"

ERROR_SAVE_DYNAMODB = "[ERROR] Unable to save to dynamodb, File: 'Request Receive', Due to '{}'"
ERROR_SAVE_SQS = "[ERROR] Unable to save to sqs, File: 'Request Receive', Due to '{}'"

def __is_api_gw( event ):
    '''check whether event is triggered by api
    '''
    # checking 
    if event and event.get("httpMethod"):
        # return
        return True
        
    # return
    return False

def __is_float( amount ):
    '''check whether the 'amount' value is convertible to float
    '''
    try:
        float( amount )
        
    except ValueError:
        # return
        return "ValueError"

def __is_invalid_char( amount ):
    '''check whether there are any invalid characters in 'amount' value
    '''
    for char in amount:
        if char not in ".0123456789":
            # return
            return "InvalidChar"
    
def __is_valid_event( event ):
    '''check and clean up event
    '''
    # define variable
    action_list = [ "withdraw", "deposit" ]
    account_type_list = [ "demo", "live" ]
    wanted_fields = [ "_id", "user_id", "robo_type", "amount", "action", "currency", "account_type" ]
    currency_list = [
                        "HUF", #HungarianForint
                        "INR", #IndianRupee
                        "TWD", #NewTaiwanDollar
                        "GBP", #Poundsterling
                        "PLN", #PolishZłoty
                        "JPY", #JapaneseYen
                        "SEK", #SwedishKrona
                        "THB", #ThaiBaht
                        "EUR", #Euro
                        "NOK", #NorwegianKrone
                        "MYR", #MalaysianRinggit
                        "CHF", #SwissFranc
                        "DKK", #DanishKrone
                        "CNY", #Renminbi
                        "IDR", #IndonesianRupiah
                        "SAR", #SaudiRiyal
                        "CAD", #CanadianDollar
                        "ISK", #IcelandicKróna
                        "PKR", #PakistaniRupee
                        "PHP", #PhilippinePeso
                        "TRY", #TurkishLira
                        "KRW", #SouthKoreanWon
                        "MXN", #MexicanPeso
                        "ILS", #IsraeliNewShekel
                        "ZAR", #SouthAfricanRand
                        "BRL", #BrazilianReal
                        "VND", #VietnameseDong
                        "GBX", #PenceSterling
                        "USD", #UnitedStatesDollar
                        "CLP", #ChileanPeso
                        "HKD", #HongKongDollar
                        "SGD", #SingaporeDollar
                    ]
    
    # check missing fields    
    missing_fields = [ fields for fields in wanted_fields if fields not in event ]
    
    if missing_fields:
        # return
        return "Missing fields: '{}'".format( missing_fields )
    
    # filter event
    event = { fields: event[fields] for fields in wanted_fields }
    
    # check datatype
    wrong_datatype = [ fields for fields, values in event.items() if not isinstance( values, str ) ]
    
    if wrong_datatype:
        # return
        return "Value of these keys must be string: '{}'".format( wrong_datatype )
    
    elif event.get("action") not in action_list:
        # return
        return "Invalid 'action' value"
    
    elif event.get("account_type") not in account_type_list:
        # return 
        return "Invalid 'account_type' value"
    
    elif event.get("currency") not in currency_list:
        # return 
        return "Invalid 'currency' value"
    
    elif __is_float( event.get("amount") ) == "ValueError":
        # return
        return "Invalid 'amount' value"
    
    elif __is_invalid_char( event.get("amount") ) == "InvalidChar":
        # return
        return "Invalid symbol in 'amount' value"

    # return
    return event
        
def get_api_event( event ):
    '''get event from body of api
    '''
    # get data
    event = event.get( "body", {} )
    event = json.loads( event )
    
    # checking
    if not isinstance( event, dict ):
        raise Exception( "Event data type is not correct, '{}'".format( str( event )[:500] ) )
    
    # return
    return event

def save2dynamodb( event ):
    '''save to dynamodb
    '''
    # define variable
    dbname = os.environ["env"]
    colname = "{}_{}".format( event.get("account_type"), os.environ["table"] )
    
    # log 
    print( DEBUG_SAVE_DYNAMODB.format( event.get("_id"), dbname, colname ) )
    
    # connect
    dynamo_db = Dynamodb_DataHandler()
    dynamo_db.connect()

    # save
    res = dynamo_db.set( dbname, colname, event )
    
    # close connection
    dynamo_db.close()

    # check saving status
    if res.get( "ResponseMetadata", {} ).get("HTTPStatusCode") == 200:
        # log
        print( INFO_SAVE_SUCCESS_DYNAMODB.format( event.get("_id"), dbname, colname ) )
        
        # return
        return True
    
    else: 
        # log
        print( WARN_SAVE_FAIL_DYNAMODB.format( event.get("_id"), dbname, colname ) )
        
        # return
        return False
        
def save2sqs( event ):
    '''save to sqs
    '''
    # define variable
    queue_name = "{}_{}_{}".format( os.environ["env"], event.get("account_type"), os.environ["queue"] )
    
    # log 
    print( DEBUG_SAVE_SQS.format( event.get("_id"), queue_name ) )

    # connect
    sqs_db = Sqs_DataHandler()
    sqs_db.connect()
    
    # save
    res = sqs_db.set( "{}_queue".format( queue_name ), "", event )

    # close connection
    sqs_db.close()

    # check saving status
    if res.get( "ResponseMetadata", {} ).get("HTTPStatusCode") == 200:
        # log
        print( INFO_SAVE_SUCCESS_SQS.format( event.get("_id"), queue_name ) )
        
        # return
        return True
    
    else: 
        # log
        print( WARN_SAVE_FAIL_SQS.format( event.get("_id"), queue_name ) )
        
        # return
        return False

def fm_init_deco( func ):
    def inner( event, context ):
        '''initialize
        '''
        # log
        print( DEBUG_START_LAMBDA )
        
        # checking
        if not isinstance( event, dict ):
            # log
            print( WARN_EVENT_NOT_DICT.format( type( event ) ) )
            
            # return
            return {
                        "statusCode": 404,
                        "body": json.dumps( "Event must be dictionary" )
                    }
        
        # return function
        return func( event, context )
    
    # return
    return inner

def fm_security_checking_deco( func ):
    def inner( event, context ):
        '''checking the input for:
        - verification
        '''
        # checking
        if __is_api_gw( event ):
            # log 
            print( INFO_API_TRIGGERED )
            
            # get event
            try:
                event = get_api_event( event )
                
            except Exception as e:
                # log
                print( WARN_GET_EVENT.format( str(e) ) )
                
                # return
                return {
                            "statusCode": 404,
                            "body": json.dumps( "Failed to get api event, Due to: '{}'".format( str(e) ) )
                        }
        
        # return function
        return func( event, context )
    
    # return
    return inner

def fm_data_checking_deco( func ):
    def inner( event, context ):
        '''checking the input for:
        - invalid data
        '''
        # checking
        if not event:
            # log
            print( WARN_NO_EVENT )
            
            # return
            return {
                        "statusCode": 204,
                        "body" : json.dumps( "No event data found" )
                    }

        # checking
        checking_res = __is_valid_event( event )
        
        if not isinstance( checking_res, dict ):
            # log
            print( WARN_INVALID_EVENT.format( checking_res ) )
            
            # return
            return {
                        "statusCode": 404,
                        "body": json.dumps( checking_res )
                    }
        
        # redefine event
        event = checking_res
        
        if event.get("_id"):
            # log
            print( DEBUG_START_LAMBDA_ID.format( event.get("_id") ) )
            
        else:
            # log
            print( WARN_START_LAMBDA_NO_ID.format( json.dumps(event) ) )
            
            # return
            return {
                        "statusCode": 404,
                        "body": json.dumps( WARN_START_LAMBDA_NO_ID.format( json.dumps(event)[:500] ) )
                    }
        
        # return function
        return func(event, context)
    
    # return
    return inner

@fm_init_deco
@fm_security_checking_deco
@fm_data_checking_deco
def main( event, context ):
    '''main
    sample input
    ------------
    # define event
    event = {
                "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e",
                "user_id": "001",
                "robo_type": "Robo_USD",
                "amount": "100",
                "action": "deposit",
                "currency": "USD",
                "account_type": "demo"
            }
    
    # sample api event (not full version)
    event = {
                "httpMethod" : "POST",
                "body": json.dumps( event )
            }
    '''
    # define variable
    dynamo_res = False
    sqs_res = False
    
    # add timestamp and status
    event["received_at"] = datetime.datetime.utcnow().strftime( "%Y-%m-%d %H:%M:%S.%f" )
    event["status"] = "received"
    
    # save to dynamodb
    try:
        dynamo_res = save2dynamodb( event )
    
    except Exception as e:
        # log
        print( ERROR_SAVE_DYNAMODB.format( str(e) ) )
    
    if dynamo_res:
        # save to queue
        try:
            sqs_res = save2sqs( event )
    
        except Exception as e:
            # log
            print( ERROR_SAVE_SQS.format( str(e) ) )
    
    # return
    if sqs_res:
        return {
                    "statusCode": 200,
                    "body": json.dumps( "Successfully saved to dynamodb and sqs, _id: '{}'".format( event.get("_id") ) )
                }
        
    else:
        return {
                    "statusCode": 404,
                    "body": json.dumps( "Fail to save to dynamodb and sqs, _id: '{}'".format( event.get("_id") ) )
                }

    
# # test run 
# # TODO: comment this before push to aws
# if __name__ == "__main__":
#     # simulate aws environment
#     os.environ["env"] = "dev"
#     os.environ["table"] = "fm_submit_req"
#     os.environ["queue"] = "fm_submit_req"
    
#     # define variable
#     event = {
#                 "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e",
#                 "user_id": "001",
#                 "robo_type": "Robo_USD",
#                 "amount": "100",
#                 "action": "deposit",
#                 "currency": "USD",
#                 "account_type": "demo"
#             }
    
#     # sample api event (not full version)
#     event = {
#                 "httpMethod" : "POST",
#                 "body": json.dumps( event )
#             }
    
#     res = main( event, "" )