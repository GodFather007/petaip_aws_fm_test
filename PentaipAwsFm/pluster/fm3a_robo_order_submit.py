"""
Des: 
    - this script will be triggered by state machine
    - this script will save the data to dynamodb and sqs to confirm order
    - state machine will pass the output from this script to the next lambda funcion 

Dependency: 
    - dynamodb -> { prod / stage / dev }_{ demo / live }_fm_submit_order
    - sqs -> { prod / stage / dev }_{ demo / live }_fm_submit_order
    - environment variables of lambda:
        os.environ["table"] = "fm_submit_order"
        os.environ["queue"] = "fm_submit_order"
        os.environ["env"] = "dev" # dev / stage / prod
        
Sample Input:
    {
          'statusCode': 200, 
          'body': '{
              "event": {
                  "user_id": "001", 
                  "action": "deposit", 
                  "currency": "USD", 
                  "status": "transferred", 
                  "received_at": "2020-10-19 08:59:10.881638", 
                  "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e", 
                  "amount": "100", 
                  "account_type": "demo", 
                  "robo_type": "Robo_USD"}, 
              "msg": "Successfully updated status, _id: \'a8098c1a-f86e-11da-bd1a-00112444be1e\', New status: \'transferred\'"
          }'
     }
    
Sample Output ( passed to next lambda function through state machine ): 
    # case #1 - confirmed
    {
         'statusCode': 200, 
         'body': '{
             "event": {
                         "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e", 
                         "user_id": "001", 
                         "robo_type": "Robo_USD", 
                         "amount": "100", 
                         "action": "deposit", 
                         "currency": "USD", 
                         "account_type": "demo", 
                         "received_at": "2020-10-19 08:59:10.881638", 
                         "status": "transferred"
                     }, 
             "new_status": "confirmed"
         }'
    }
    
    # case #2 - pending reverse
    {
         'statusCode': 202, 
         'body': '{
             "event": {
                         "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e", 
                         "user_id": "001", 
                         "robo_type": "Robo_USD", 
                         "amount": "1000000000", 
                         "action": "deposit", 
                         "currency": "USD", 
                         "account_type": "demo", 
                         "received_at": "2020-10-19 08:59:10.881638", 
                         "status": "transferred"
                     }, 
             "new_status": "pending reverse", 
             "error_msg": "Balance insufficient, Currency: \'USD\'"
         }'
    }
    
Sample Data ( data/msg saved to dynamodb/sqs ):
    {
         'user_id': '001', 
         'action': 'deposit', 
         'currency': 'USD', 
         'status': 'transferred', 
         'received_at': '2020-10-19 08:59:10.881638', 
         '_id': 'a8098c1a-f86e-11da-bd1a-00112444be1e', 
         'amount': '100', 
         'account_type': 'demo', 
         'robo_type': 'Robo_USD'
     }
    
Author:
    Tessy
    
Log:
    Tessy   17 Oct 2020     First version of this script
    Tessy   20 Oct 2020     Added some checking and made some amendments to the logging
    
Status: 
    Alpha
"""

# import libs
import os
import json
try:
    from PentaipAwsFm.pluster.pluster_core.sqs_handler import DataHandler as Sqs_DataHandler
    from PentaipAwsFm.pluster.pluster_core.dynamodb_handler import DataHandler as Dynamodb_DataHandler
except:
    from pluster_core.sqs_handler import DataHandler as Sqs_DataHandler
    from pluster_core.dynamodb_handler import DataHandler as Dynamodb_DataHandler
  
# log messages
INFO_SF_TRIGGERED = "[INFO] Triggered by step function, File: 'Robo Order Submit'"
INFO_SAVE_SUCCESS_DYNAMODB = "[INFO] Saved success to dynamodb, File: 'Robo Order Submit', _id: '{}', Table: '{}_{}'"
INFO_SAVE_SUCCESS_SQS = "[INFO] Saved success to sqs, File: 'Robo Order Submit', _id: '{}', Queue: '{}_queue'"

DEBUG_START_LAMBDA = "[DEBUG] Start lambda, File: 'Robo Order Submit'"
DEBUG_SAVE_DYNAMODB = "[DEBUG] Saving to dynamodb, File: 'Robo Order Submit', _id: '{}'"
DEBUG_SAVE_SQS = "[DEBUG] Saving to sqs, File: 'Robo Order Submit', _id: '{}', Queue: '{}_queue'"

WARN_STATUS_CODE = "[WARNING] Invalid status code, File: 'Robo Order Submit', Status code: '{}'"
WARN_GET_EVENT = "[WARNING] Failed to get event, File: 'Robo Order Submit', Due to: '{}'"
WARN_NO_EVENT = "[WARNING] No event data found, File: 'Robo Order Submit'"
WARN_EVENT_NOT_DICT = "[WARNING] Event is not dictionary, File: 'Robo Order Submit', Datatype: '{}'"
WARN_SAVE_FAIL_DYNAMODB = "[WARNING] Saved fail to dynamodb, File: 'Robo Order Submit', _id: '{}', Table: '{}_{}'"
WARN_SAVE_FAIL_SQS = "[WARNING] Saved fail to sqs, File: 'Robo Order Submit', _id: '{}', Queue: '{}_queue'"

ERROR_SAVE_DYNAMODB = "[ERROR] Unable to save to dynamodb, File: 'Robo Order Submit', Due to '{}'"
ERROR_SAVE_SQS = "[ERROR] Unable to save to sqs, File: 'Robo Order Submit', Due to '{}'"

def __is_sf( event ):
    '''check whether event is triggered by step function
    '''
    # checking 
    if event and event.get("statusCode"):
        # return
        return True
        
    # return
    return False

def __is_valid_status_code( event ):
    '''check status code
    '''
    # checking
    return True if event.get("statusCode") in [ 200 ] else False

def get_sf_event( event ):
    '''get event from body of sf
    '''
    # get data
    event = event.get( "body", {} )
    event = json.loads( event )
    
    # return
    return event.get( "event", {} )
    
def save2dynamodb( event ):
    '''save to dynamodb
    '''
    # define variable
    dbname = os.environ["env"]
    colname = "{}_{}".format( event.get("account_type"), os.environ["table"] )
    
    # log 
    print( DEBUG_SAVE_DYNAMODB.format( event.get("_id"), dbname, colname ) )
    
    # connect
    dynamo_db = Dynamodb_DataHandler()
    dynamo_db.connect()

    # save
    res = dynamo_db.set( dbname, colname, event )
    
    # close connection
    dynamo_db.close()

    # check saving status
    if res["ResponseMetadata"].get("HTTPStatusCode") == 200:
        # log
        print( INFO_SAVE_SUCCESS_DYNAMODB.format( event.get("_id"), dbname, colname ) )
        
        # return
        return True
    
    else: 
        # log
        print( WARN_SAVE_FAIL_DYNAMODB.format( event.get("_id"), dbname, colname ) )
        
        # return
        return False
        
def save2sqs( event ):
    '''save to sqs
    '''
    # define variable
    queue_name = "{}_{}_{}".format( os.environ["env"], event.get("account_type"), os.environ["queue"] )
    
    # log 
    print( DEBUG_SAVE_SQS.format( event.get("_id"), queue_name ) )

    # connect
    sqs_db = Sqs_DataHandler()
    sqs_db.connect()
    
    # save
    res = sqs_db.set( "{}_queue".format( queue_name ), "", event )

    # close connection
    sqs_db.close()

    # check saving status
    if res["ResponseMetadata"].get("HTTPStatusCode") == 200:
        # log
        print( INFO_SAVE_SUCCESS_SQS.format( event.get("_id"), queue_name ) )
        
        # return
        return True
    
    else: 
        # log
        print( WARN_SAVE_FAIL_SQS.format( event.get("_id"), queue_name ) )
        
        # return
        return False
    
def fm_init_deco( func ):
    def inner( event, context ):
        '''initialize
        '''
        # log
        print( DEBUG_START_LAMBDA )
        
        # checking
        if not isinstance( event, dict ):
            # log
            print( WARN_EVENT_NOT_DICT.format( type( event ) ) )
            
            # return
            return {
                        "statusCode": 404,
                        "body": json.dumps( "Event must be dictionary" )
                    }
        
        # return function
        return func( event, context )
    
    # return
    return inner
    
def fm_security_checking_deco( func ):
    def inner( event, context ):
        '''checking the input for:
        - verification
        '''
        if __is_sf( event ):
            # log
            print( INFO_SF_TRIGGERED )
            
            if not __is_valid_status_code( event ):        
                # log
                print( WARN_STATUS_CODE.format( event.get("statusCode") ) )
                
                # return
                return {
                            "statusCode": 404,
                            "body" : json.dumps( "Invalid status code" )
                        }
            
            else:
                # get event
                try:
                    event = get_sf_event( event )
                    
                except Exception as e:
                    # log
                    print( WARN_GET_EVENT.format( str(e) ) )
                    
                    # return
                    return {
                                "statusCode": 404,
                                "body": json.dumps( "Failed to get sf event, Due to: '{}'".format( str(e) ) )
                            }
                
        # return function
        return func( event, context )
    
    # return
    return inner

def fm_data_checking_deco( func ):
    def inner( event, context ):
        '''checking the input for:
        - invalid data
        '''
        # checking
        if not event:
            # log
            print( WARN_NO_EVENT )
            
            # return
            return {
                        "statusCode": 204,
                        "body" : json.dumps( "No event data found" )
                    }
    
        if not isinstance( event, dict ):
            # log
            print( WARN_EVENT_NOT_DICT.format( type( event ) ) )
            
            # return
            return {
                        "statusCode": 404,
                        "body" : json.dumps( "Event must be dictionary" )
                    }
        
        # return function
        return func(event, context)
        
    # return
    return inner

@fm_init_deco
@fm_security_checking_deco
@fm_data_checking_deco
def main( event, context ):
    '''main
    sample input
    ------------
    # define event
    event = {
                  "user_id": "001",
                  "action": "deposit",
                  "currency": "USD",
                  "status": "transferred",
                  "received_at": "2020-10-19 08:59:10.881638",
                  "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e",
                  "amount": "100",
                  "account_type": "demo",
                  "robo_type": "Robo_USD"
            }
    
    # sample step function event ( status = transferred )
    event =     {
                    "statusCode": 200,
                    "body": json.dumps( {
                        "event": event,
                        "msg": "Successfully updated status, _id: '{}', New status: '{}'".format( event.get("_id"), event.get("status") )
                    } )
                }
    '''
    # define variable
    dynamo_res = False
    sqs_res = False
    
    # save to dynamodb
    try:
        dynamo_res = save2dynamodb( event )
    
    except Exception as e:
        # log
        print( ERROR_SAVE_DYNAMODB.format( str(e) ) )
    
    if dynamo_res:
        # save to queue
        try:
            sqs_res = save2sqs( event )
    
        except Exception as e:
            # log
            print( ERROR_SAVE_SQS.format( str(e) ) )
    
    # return
    if sqs_res:
        return {
                    "statusCode": 200,
                    "body": json.dumps( {
                        "event": event,
                        "new_status": "confirmed",
                    } )
                }
        
    else:
        return {
                    "statusCode": 202,
                    "body": json.dumps( {
                        "event": event,
                        "new_status": "pending reverse",
                        "error_msg" :  "Fail to save to dynamodb and sqs, _id: '{}'".format( event.get("_id") )
                    } )
                }
    

# # test run 
# # TODO: comment this before push to aws
# if __name__ == "__main__":
#     # simulate aws environment
#     os.environ["env"] = "dev"
#     os.environ["table"] = "fm_submit_order"
#     os.environ["queue"] = "fm_submit_order"
        
#     # define variable
#     event = {
#                   "user_id": "001",
#                   "action": "deposit",
#                   "currency": "USD",
#                   "status": "transferred",
#                   "received_at": "2020-10-19 08:59:10.881638",
#                   "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e",
#                   "amount": "100",
#                   "account_type": "demo",
#                   "robo_type": "Robo_USD"
#             }
    
#     # sample step function event ( status = transferred )
#     event =     {
#                     "statusCode": 200,
#                     "body": json.dumps( {
#                         "event": event,
#                         "msg": "Successfully updated status, _id: '{}', New status: '{}'".format( event.get("_id"), event.get("status") )
#                     } )
#                 }
    
#     res = main( event, "" )