"""
Des: 
    - this script will be triggered by state machine
    - this script checks whether user have sufficient balance to invest
    - state machine will pass the output from this script to the next lambda funcion 

Dependency: 
    - dynamodb { prod / stage / dev }_{ demo / live }_user_wallet
    - environment variables of lambda:
        os.environ["user_table"] = "user_wallet"
        os.environ["env"] = "dev" # dev / stage / prod
        os.environ["update_table"] = "fm_submit_req"
        os.environ["update_queue"] = "fm_pending_transfer"
        os.environ["admin_table"] = "error_orders"
        
Sample Input:
    # event passed from step function
    {
         '_id': 'a8098c1a-f86e-11da-bd1a-00112444be1e', 
         'user_id': '001', 
         'robo_type': 'Robo_USD', 
         'amount': '100', 
         'action': 'deposit', 
         'currency': 'USD', 
         'account_type': 'demo', 
         'received_at': '2020-10-19 08:59:10.881638', 
         'status': 'received'
     }
        
Sample Output ( passed to next lambda function through state machine ): 
    # case #1 - sufficient
    {
         'statusCode': 200, 
         'body': '{
             "event": {
                         "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e", 
                         "user_id": "001", 
                         "robo_type": "Robo_USD", 
                         "amount": "100", 
                         "action": "deposit", 
                         "currency": "USD", 
                         "account_type": "demo", 
                         "received_at": "2020-10-19 08:59:10.881638", 
                         "status": "received"
                     }, 
             "new_status": "sufficient"
         }'
    }
    
    # case #2 - rejected
    {
         'statusCode': 202, 
         'body': '{
             "event": {
                         "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e", 
                         "user_id": "001", 
                         "robo_type": "Robo_USD", 
                         "amount": "1000000000", 
                         "action": "deposit", 
                         "currency": "USD", 
                         "account_type": "demo", 
                         "received_at": "2020-10-19 08:59:10.881638", 
                         "status": "received"
                     }, 
             "new_status": "rejected", 
             "error_msg": "Balance insufficient, Currency: \'USD\'"
         }'
    }
    
Author:
    Tessy
    
Log:
    Tessy   14 Oct 2020     First version of this script
    Tessy   19 Oct 2020     Added some checking and made some amendments to the logging
    
Status: 
    Alpha
"""

# import libs
import os
import sys
import json
import uuid
import traceback
try:
    from PentaipAwsFm.pluster.pluster_core.sqs_handler import DataHandler as Sqs_DataHandler
    from PentaipAwsFm.pluster.pluster_core.dynamodb_handler import DataHandler as Dynamodb_DataHandler
except:
    from pluster_core.sqs_handler import DataHandler as Sqs_DataHandler
    from pluster_core.dynamodb_handler import DataHandler as Dynamodb_DataHandler
  
# log messages
INFO_SUFFICIENT_BAL = "[INFO] User has sufficient balance in wallet, File: 'Request Check', User id: '{}'"
INFO_UPDATE_SUCCESS_DYNAMODB = "[INFO] Updated success to dynamodb, File: 'Request Check', _id: '{}', Table: '{}_{}', New status: '{}'"
INFO_SAVE_SUCCESS_SQS = "[INFO] Saved success to sqs, File: 'Request Check', _id: '{}', Queue: '{}_queue'"

DEBUG_START_LAMBDA = "[DEBUG] Start lambda, File: 'Request Check'"
DEBUG_START_LAMBDA_ID = "[DEBUG] Start lambda by ID, File: 'Request Check', _id: '{}', Action: 'Check balance'"
DEBUG_BAL_SUFFICIENT_ID = "[DEBUG] Start lambda by ID, File: 'Request Check', _id: '{}', Action: 'Balance sufficient'"
DEBUG_BAL_INSUFFICIENT_ID = "[DEBUG] Start lambda by ID, File: 'Request Check', _id: '{}', Action: 'Balance insufficient'"
DEBUG_ADMIN_ERROR_ID = "[DEBUG] Start lambda by ID, File: 'Request Check', _id: '{}', Action: 'Admin error'"
DEBUG_GET_USER_BAL = "[DEBUG] Getting user balance, File: 'Request Check', _id: '{}', Table: '{}_{}'"
DEBUG_UPDATE_DYNAMODB = "[DEBUG] Updating to dynamodb, File: 'Request Check', _id: '{}', Table: '{}_{}', New status: '{}'"
DEBUG_SAVE_SQS = "[DEBUG] Saving to sqs, File: 'Request Check', _id: '{}', Queue: '{}_queue'"

WARN_NO_RECORD = "[WARNING] User has no existing wallet record in database, File: 'Request Check', User id: '{}'"
WARN_INSUFFICIENT_BAL = "[WARNING] User has insufficient balance in wallet, File: 'Request Check', User id: '{}', User balance: '{}', Deposit amount: '{}'"
WARN_UPDATE_FAIL_DYNAMODB = "[WARNING] Updated fail to dynamodb, File: 'Request Check', _id: '{}', Table: '{}_{}', New status: '{}'"
WARN_SAVE_FAIL_SQS = "[WARNING] Saved fail to sqs, File: 'Request Check', _id: '{}', Queue: '{}_queue'"

ERROR_CHECK_USER_BAL = "[ERROR] Failed to check user balance, File: 'Request Check', Due to: '{}'"
ERROR_UPDATE_DYNAMODB = "[ERROR] Unable to update status to dynamodb, File: 'Request Check', Due to: '{}'"
ERROR_ORDER_FOUND = "[ERROR] Error order happening, informing admin in database, File: 'Request Check', Status: 'Found', Event: '{}'"
ERROR_ORDER_REPORT = "[ERROR] Error order happening, informing admin in database, File: 'Request Check', Status: 'Update', Event: '{}'"
  
def __is_sf( event ):
    '''check whether event is triggered by step function
    '''
    # checking 
    if event and event.get("statusCode"):
        # return
        return True
        
    # return
    return False

def get_sf_event( event ):
    '''get event from body of sf
    '''
    # get data
    event = event.get( "body", {} )
    event = json.loads( event )
    
    # return
    return event
   
def __is_balance_sufficient( event, user_balance ):
    '''check whether user has sufficient balance to invest
    '''
    # check and return
    return True if float( user_balance.get("balance") ) >= float( event.get("amount") ) else False

def get_user_balance( event, _id, dynamo_db ):
    '''get user balance data
    '''
    # define variable
    dbname = os.environ["env"]
    colname = "{}_{}".format( event.get("account_type"), os.environ["user_table"] )
    
    # log 
    print( DEBUG_GET_USER_BAL.format( _id, dbname, colname ) )

    # get and return data
    return dynamo_db.get( dbname, colname, _id )
    
def __update_ddb_req_order_set( event, new_status, error_msg = None ):
    # define variable
    dbname = os.environ["env"]
    colname = "{}_{}".format( event.get("account_type"), os.environ["update_table"] )
    result = {
                    "statusCode": 404,
                    "body": json.dumps( {
                        "event": event,
                        "new_status": "rejected",
                        "error_msg" : error_msg or "Fail to update record to database when is '{}' status, might be database client is overload".format( new_status )
                    } )
                }
    
    # log 
    print( DEBUG_UPDATE_DYNAMODB.format( event.get("_id"), dbname, colname, new_status ) )

    # connect
    dynamo_db = Dynamodb_DataHandler()
    dynamo_db.connect()
    
    # get data
    data = dynamo_db.get( dbname, colname, event.get("_id") )

    # set data
    if data and data.get( "status", "" ) in [ "received", "sufficient" ]:
        # update status
        data["status"] = new_status
        if error_msg:
            data["error_msg"] = error_msg

        # update status
        update_res = dynamo_db.set( dbname, colname, data )
        
        # checking
        if update_res.get( "ResponseMetadata", {} ).get("HTTPStatusCode") == 200:
            # log
            print( INFO_UPDATE_SUCCESS_DYNAMODB.format( data.get("_id"), dbname, colname, new_status ) )

            result = {
                        "statusCode": 200,
                        "body": json.dumps( {
                            "event": data,
                            "new_status": new_status,
                        } )
                    }
            
        else: 
            # log
            print( WARN_UPDATE_FAIL_DYNAMODB.format( data.get("_id"), dbname, colname, new_status ) )

    # close connection
    dynamo_db.close()

    # return
    return result

def __update_sqs_req_order_set( result, new_status ):
    '''save to sqs
    remark: concurrency default 50 because SQS write in is 150/per second resource limit
    '''
    # define variable
    event = get_sf_event( result ).get("event", {})
    queue_name = "{}_{}_{}".format( os.environ["env"], event.get("account_type"), os.environ["update_queue"] )
    fail_result = {
                        "statusCode": 404,
                        "body": json.dumps( {
                            "event": event,
                            "new_status": "rejected",
                            "error_msg" : "Fail to update record to queue when is '{}' status, might be queue client is overload".format( new_status )
                        } )
                    }
    
    # checking
    if not event:
        return fail_result
    
    # log 
    print( DEBUG_SAVE_SQS.format( event.get("_id"), queue_name ) )

    # connect
    sqs_db = Sqs_DataHandler()
    sqs_db.connect()
    
    # save
    res = sqs_db.set( "{}_queue".format( queue_name ), "", event )

    # close connection
    sqs_db.close()

    # check saving status
    if res.get( "ResponseMetadata", {} ).get("HTTPStatusCode") == 200:
        # log
        print( INFO_SAVE_SUCCESS_SQS.format( event.get("_id"), queue_name ) )
        
        # return
        return result
    
    else: 
        # log
        print( WARN_SAVE_FAIL_SQS.format( event.get("_id"), queue_name ) )
        
        # return
        return fail_result
    
def update_req_order_sufficient( event ):
    '''update order status from received to sufficient
    '''
    # define variable
    new_status = "sufficient"

    # set data to db
    result = __update_ddb_req_order_set( event, new_status )

    # set data to sqs
    if result["statusCode"] == 200:
        result = __update_sqs_req_order_set( result, new_status )

    # return
    return result

def update_req_order_insufficient( event, error_msg ):
    '''update order status from received to sufficient
    '''
    # define variable
    new_status = "rejected"

    # set data
    result = __update_ddb_req_order_set( event, new_status, error_msg )

    # return
    if result.get( "statusCode" ) == 200:
        return result

    else:
        raise Exception( json.dumps( result ) )


def fm_init_deco( func ):
    def inner( event, context ):
        '''initialize
        '''
        # log
        print( DEBUG_START_LAMBDA )
        
        # return function
        return func( event, context )
    
    # return
    return inner

def fm_security_checking_deco( func ):
    def inner( event, context ):
        '''checking the input for:
        - verification
        '''
        # return function
        return func( event, context )
    
    # return
    return inner

def fm_data_checking_deco( func ):
    def inner( event, context ):
        '''checking the input for:
        - invalid data
        '''
        # log
        print( DEBUG_START_LAMBDA_ID.format( event.get("_id") ) )
        
        # return function
        return func(event, context)
    
    # return
    return inner

def fm_balance_sufficient_deco( func ):
    def inner( event, context ):
        '''get event body from step function status
        '''
        # log
        print( DEBUG_BAL_SUFFICIENT_ID.format( event.get("_id") ) )
        
        # get
        data = get_sf_event( event )
        
        # checking
        assert data, "Invalid data in 'fm_balance_sufficient_deco', Event: '{}'".format( event )
            
        # return function
        return func(data, context)
    
    # return
    return inner

def fm_balance_insufficient_deco( func ):
    def inner( event, context ):
        '''get event body from step function status
        '''
        # get
        data = get_sf_event( event )
        
        # log
        print( DEBUG_BAL_INSUFFICIENT_ID.format( data.get( "event", {} ).get("_id") ) )
        
        # checking
        assert data, "Invalid data in 'fm_balance_insufficient_deco', Event: '{}'".format( event )
            
        # return function
        return func(data, context)
    
    # return
    return inner

def fm_admin_notice_error_deco( func ):
    def inner( event, context ):
        '''get event body from step function status
        '''
        # get
        data = get_sf_event( event )
        
        # log
        print( DEBUG_ADMIN_ERROR_ID.format( data.get( "event", {} ).get("_id") ) )
        
        # checking
        # expected behavior: if sf fail 3 times, go to admin_order_errors table
        assert data, "Invalid data in 'fm_admin_notice_error_deco', Event: '{}'".format( event )
            
        # return function
        return func(data, context)
    
    # return
    return inner

@fm_init_deco
@fm_security_checking_deco
@fm_data_checking_deco
def check_balance( event, context ):
    '''check balance lambda function
    sample input
    ------------
    # define event
    event = {
                "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e",
                "user_id": "001",
                "robo_type": "Robo_USD",
                "amount": "100",
                "action": "deposit",
                "currency": "USD",
                "account_type": "demo",
                "received_at": "2020-10-19 08:59:10.881638",
                "status": "received"
            }
    '''
    # define variable
    balance_res = False
    _id = "{}_{}".format( event.get("user_id"), event.get("currency") ) 
    
    # connect
    dynamo_db = Dynamodb_DataHandler()
    dynamo_db.connect()
    
    # get user balance
    user_balance = get_user_balance( event, _id, dynamo_db )
    
    # close connection
    dynamo_db.close()
    
    # checking
    if not user_balance:
        # log
        print( WARN_NO_RECORD.format( _id ) )
        
        # return
        return {
                    "statusCode": 202,
                    "body": json.dumps( {
                        "event": event,
                        "new_status": "rejected",
                        "error_msg" : "User has no existing wallet record in database OR database client is overload"
                    } )
                }
        
    # checking balance
    try:
        balance_res = __is_balance_sufficient( event, user_balance )

    except Exception as e:
        # log
        print( ERROR_CHECK_USER_BAL.format( str(e) ) )
        
        # return 
        return {
                    "statusCode": 202,
                    "body": json.dumps( {
                        "event": event,
                        "new_status": "rejected",
                        "error_msg" : "Unable to check user balance, Due to: '{}'".format( str(e) )
                    } )
                }
    
    if balance_res:
        # log
        print( INFO_SUFFICIENT_BAL.format( _id ) )
        
        # return 
        return {
                    "statusCode": 200,
                    "body": json.dumps( {
                        "event": event,
                        "new_status": "sufficient",
                    } )
                }
    else:
        # log
        print( WARN_INSUFFICIENT_BAL.format( _id, user_balance.get("balance"), event.get("amount") ) )
        
        # return 
        return {
                    "statusCode": 202,
                    "body": json.dumps( {
                        "event": event,
                        "new_status": "rejected",
                        "error_msg" : "Balance insufficient, Currency: '{}'".format( event.get("currency") )
                    } )
                }
    
@fm_balance_sufficient_deco
def update_accepted_req_order_status( event, context ):
    '''update 'status' from 'received' to 'sufficient'
    '''
    # define variable
    event = event.get("event")
    
    try:
        update_req_order_sufficient( event )
        
    except Exception as e:
        # log 
        exception_type, exception_value, exception_traceback = sys.exc_info()
        traceback_string = traceback.format_exception(exception_type, exception_value, exception_traceback)
        err_msg = json.dumps({
            "errorType": exception_type.__name__,
            "errorMessage": str(exception_value),
            "stackTrace": traceback_string
        })
        
        print( ERROR_UPDATE_DYNAMODB.format( err_msg ) )
        
        raise e
        
    # return
    return

@fm_balance_insufficient_deco
def update_rejected_req_order_status( event, context ):
    '''update 'status' from 'received' to 'rejected'
    '''
    # define variable
    event, error_msg = event.get("event"), event.get("error_msg")
    
    try:
        update_req_order_insufficient( event, error_msg )
    
    except Exception as e:
        # log 
        exception_type, exception_value, exception_traceback = sys.exc_info()
        traceback_string = traceback.format_exception(exception_type, exception_value, exception_traceback)
        err_msg = json.dumps({
            "errorType": exception_type.__name__,
            "errorMessage": str(exception_value),
            "stackTrace": traceback_string
        })
        
        print( ERROR_UPDATE_DYNAMODB.format( err_msg ) )
        
        raise e
        
    # return
    return

@fm_admin_notice_error_deco
def update_admin_error_oder( event, context ):
    '''notification to admin there is error order
    
    aurthur: yeong, 2020-10-31, skeleton & comment
    
    input:
        event:
            {
                "xxxx":"xxxx",
                "xxxx":"xxxx",
                "xxxx":"xxxx"
            }
        context:
            Amazon lambda's context
    
    dnamodb table name: [dev|stage|prod]_error_orders
        
    data format:
        {
            "xxxx":"xxxx",
            "xxxx":"xxxx",
            "xxxx":"xxxx"
        }
        
    return: None
    '''
    # define variable
    dbname = os.environ["env"]
    colname = os.environ["admin_table"]
    
    # log 
    print( ERROR_ORDER_FOUND.format( json.dumps( event ) ) )
          
    # define data
    data = {
                "_id": str(uuid.uuid4()),
                **event
            }

    # connect
    dynamo_db = Dynamodb_DataHandler()
    dynamo_db.connect()

    # set data
    res = dynamo_db.set( dbname, colname, data )

    # close connection
    dynamo_db.close()
    
    # log 
    if res.get( "ResponseMetadata", {} ).get("HTTPStatusCode") == 200:
        print( ERROR_ORDER_REPORT.format( json.dumps( event ) ) )

    # return
    return

# # test run 
# # TODO: comment this before push to aws
# if __name__ == "__main__":
#     # simulate aws environment
#     os.environ["env"] = "dev"
#     os.environ["user_table"] = "user_wallet"
#     os.environ["update_table"] = "fm_submit_req"
#     os.environ["update_queue"] = "fm_pending_transfer"
#     os.environ["admin_table"] = "error_orders"
        
#     # define variable
#     event = {
#                 "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e",
#                 "user_id": "001",
#                 "robo_type": "Robo_USD",
#                 "amount": "100",
#                 "action": "deposit",
#                 "currency": "USD",
#                 "account_type": "demo",
#                 "received_at": "2020-10-19 08:59:10.881638",
#                 "status": "received"
#             }
    
#     res = main( event, "" )