"""
Des: 
    - this script will be triggered by sqs event source mapping
    - this script will transfer the investment amount from "user_wallet" to "robo_wallet"
    - this script will trigger state machine
    - if fail to trigger state machine ( high possibility is due to 'ThrottlingException' ), will update status to "reject" in this script, then the flow ends here

Dependency: 
    - dynamodb -> { prod / stage / dev }_{ demo / live }_user_wallet
    - dynamodb -> { prod / stage / dev }_{ demo / live }_robo_wallet
    - dynamodb -> { prod / stage / dev }_{ demo / live }_fm_submit_req
    - sf -> { prod / stage / dev }_fm_order_submit_state_machine
    - environment variables of lambda:
        os.environ["env"] = "dev" # dev / stage / prod
        os.environ["user_table"] = "user_wallet"
        os.environ["robo_table"] = "robo_wallet"
        os.environ["sf_name"] = "fm_order_submit_state_machine"
        os.environ["update_table"] = "fm_submit_req"
      
Sample Input:
    # sample msg passed from sqs
    event = {
                'Records': [{
                    'messageId': '5050fd45-327b-4923-ac48-d37388b63549', 
                    'receiptHandle': 'AQEBvTE3iiVC ... bLvN7R8JU1FK8AnS97xEmo=', 
                    'body': '{
                          "user_id": "001",
                          "action": "deposit",
                          "currency": "USD",
                          "status": "sufficient",
                          "received_at": "2020-10-19 08:59:10.881638",
                          "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e",
                          "amount": "100",
                          "account_type": "demo",
                          "robo_type": "Robo_USD"
                    }', 
                    'attributes': {'ApproximateReceiveCount': '1', 'SentTimestamp': '1602210795408', 'SenderId': 'AROAYOWNWXFAO6AW7MZIM:PentaipAwsFmDevelop-FmLam-Fm1aRequestReceiveFunction-1U34J434LIJN9', 'ApproximateFirstReceiveTimestamp': '1602210795409'}, 
                    'messageAttributes': {}, 
                    'md5OfBody': '850041fff21e3c8e1d86a8b940b63570', 
                    'eventSource': 'aws:sqs', 'eventSourceARN': 'arn:aws:sqs:us-east-2:581325732160:dev_demo_fm_pending_transfer_queue', 'awsRegion': 'us-east-2'
                    }]
                }
    
    # sample input for API
    {
        "user_id": "001",
        "action": "deposit",
        "currency": "USD",
        "status": "sufficient",
        "received_at": "2020-10-19 08:59:10.881638",
        "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e",
        "amount": "100",
        "account_type": "demo",
        "robo_type": "Robo_USD"
    }
    
    # sample api event passed into this script (not full version)
    event = {
                "httpMethod" : "POST",
                "body": json.dumps( event )
            }
    
Sample Output: 
    {'statusCode': 200, 'body': '"Step function triggered success, _id: \'a8098c1a-f86e-11da-bd1a-00112444be1e\'"'}
    
Sample Data ( data passed to step function ):
    # case #1 - transferred
    {
         'statusCode': 200, 
         'body': '{
             "event": {
                         "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e", 
                         "user_id": "001", 
                         "robo_type": "Robo_USD", 
                         "amount": "100", 
                         "action": "deposit", 
                         "currency": "USD", 
                         "account_type": "demo", 
                         "received_at": "2020-10-19 08:59:10.881638", 
                         "status": "sufficient"
                     }, 
             "new_status": "transferred"
         }'
    }
    
    # case #2 - rejected
    {
         'statusCode': 202, 
         'body': '{
             "event": {
                         "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e", 
                         "user_id": "001", 
                         "robo_type": "Robo_USD", 
                         "amount": "1000000000", 
                         "action": "deposit", 
                         "currency": "USD", 
                         "account_type": "demo", 
                         "received_at": "2020-10-19 08:59:10.881638", 
                         "status": "sufficient"
                     }, 
             "new_status": "rejected", 
             "error_msg": "Balance insufficient, Currency: \'USD\'"
         }'
    }
                        
Author:
    Tessy
    
Log:
    Tessy   14 Oct 2020     First version of this script
    Tessy   20 Oct 2020     Added some checking and made some amendments to the logging
    
Status: 
    Alpha
"""

# import libs
import os
import json
import boto3
try:
    from PentaipAwsFm.pluster.pluster_core.dynamodb_handler import DataHandler as Dynamodb_DataHandler
except:
    from pluster_core.dynamodb_handler import DataHandler as Dynamodb_DataHandler
  
# log messages
INFO_SQS_TRIGGERED = "[INFO] Triggered by sqs, File: 'Principal Transfer'"
INFO_API_TRIGGERED = "[INFO] Triggered by api, File: 'Principal Transfer'"
INFO_UPDATE_USER_SUCCESS_DYNAMODB = "[INFO] Updated success to dynamodb for user bal, File: 'Principal Transfer', _id: '{}', Table: '{}_{}', New balance: '{}'"
INFO_UPDATE_ROBO_SUCCESS_DYNAMODB = "[INFO] Updated success to dynamodb for robo bal, File: 'Principal Transfer', _id: '{}', Table: '{}_{}', New balance: '{}'"
INFO_TRIGGER_SUCCESS_SF = "[INFO] Trigger success step function, File: 'Principal Transfer', _id:'{}', Step function: '{}'"
INFO_UPDATE_SUCCESS_DYNAMODB = "[INFO] Updated success to dynamodb, File: 'Principal Transfer', _id: '{}', Table: '{}_{}', New status: '{}'"

DEBUG_START_LAMBDA = "[DEBUG] Start lambda, File: 'Principal Transfer'"
DEBUG_GET_USER_BAL = "[DEBUG] Getting user balance, File: 'Principal Transfer', _id: '{}', Table: '{}_{}'"
DEBUG_GET_ROBO_BAL = "[DEBUG] Getting robo balance, File: 'Principal Transfer', _id: '{}', Table: '{}_{}'"
DEBUG_UPDATE_USER_DYNAMODB = "[DEBUG] Updating user bal to dynamodb, File: 'Principal Transfer', _id: '{}', Table: '{}_{}', New balance: '{}'"
DEBUG_UPDATE_ROBO_DYNAMODB = "[DEBUG] Updating robo bal to dynamodb, File: 'Principal Transfer', _id: '{}', Table: '{}_{}', New balance: '{}'"
DEBUG_TRIGGER_SF = "[DEBUG] Triggering step function, File: 'Principal Transfer', _id: '{}', Step function: '{}'"
DEBUG_UPDATE_DYNAMODB = "[DEBUG] Updating to dynamodb, File: 'Principal Transfer', _id: '{}', Table: '{}_{}', New status: '{}'"

WARN_BATCH_SIZE = "[WARNING] Batch size is not 1, File: 'Principal Transfer', Batch Size: '{}'"
WARN_GET_EVENT = "[WARNING] Failed to get event, File: 'Principal Transfer', Due to: '{}'"
WARN_NO_EVENT = "[WARNING] No event data found, File: 'Principal Transfer'"
WARN_ZERO_AMOUNT = "[WARNING] Amount is zero or negative, File: 'Principal Transfer'"
WARN_EVENT_NOT_DICT = "[WARNING] Event is not dictionary, File: 'Principal Transfer', Datatype: '{}'"
WARN_NO_USER_RECORD = "[WARNING] User has no existing wallet record in database, File: 'Principal Transfer', _id: '{}'"
WARN_NO_ROBO_RECORD = "[WARNING] Robo has no existing wallet record in database, File: 'Principal Transfer', _id: '{}'"
WARN_TRANSFER_BAL = "[WARNING] Unable to transfer balance from user to robo wallet, File: 'Principal Transfer', Due to: '{}'"
WARN_BAL_NOT_UPDATED = "[WARNING] Robo/User balance not updated, File: 'Principal Transfer', Ori robo balance: '{}', Updated robo balance: '{}', Ori user balance: '{}', Updated user balance: '{}'"
WARN_UPDATE_USER_FAIL_DYNAMODB = "[WARNING] Updated fail to dynamodb for user bal, File: 'Principal Transfer', _id: '{}', Table: '{}_{}', New balance: '{}'"
WARN_UPDATE_ROBO_FAIL_DYNAMODB = "[WARNING] Updated fail to dynamodb for robo bal, File: 'Principal Transfer', _id: '{}', Table: '{}_{}', New balance: '{}'"
WARN_TRIGGER_FAIL_SF = "[WARNING] Trigger fail step function, File: 'Principal Transfer', _id: '{}', Step function: '{}'"
WARN_UPDATE_FAIL_DYNAMODB = "[WARNING] Updated fail to dynamodb, File: 'Principal Transfer', _id: '{}', Table: '{}_{}', New status: '{}'"

ERROR_UPDATE_USER_DYNAMODB = "[ERROR] Unable to update user bal to dynamodb, File: 'Principal Transfer', Due to: '{}'"
ERROR_UPDATE_ROBO_DYNAMODB = "[ERROR] Unable to update robo bal to dynamodb, File: 'Principal Transfer', Due to: '{}'"
ERROR_TRIGGER_SF = "[ERROR] Unable to trigger step function, File: 'Principal Transfer', _id: '{}', Step function: '{}', Due to: '{}'"
ERROR_UPDATE_DYNAMODB = "[ERROR] Unable to update status to dynamodb, File: 'Principal Transfer', Due to: '{}'"

def __is_valid_batch_size( event ):
    '''check batch size
    '''
    # checking
    return True if event and len( event.get("Records") ) == 1 else False

def __is_sqs( event ):
    '''check whether event is triggered by sqs
    '''
    # checking
    if event.get("Records") and event["Records"][0].get("eventSource") == "aws:sqs":
        # return
        return True
    
    # return
    return False

def __is_api_gw( event ):
    '''check whether event is triggered by api
    '''
    # checking 
    if event and event.get("httpMethod"):
        # return
        return True
        
    # return
    return False

def get_sqs_event( event ):
    '''get event from body of sqs
    '''
    # get data
    event = event["Records"][0].get( "body", {} )
    event = json.loads( event )
    
    # return
    return event
        
def get_api_event( event ):
    '''get event from body of api
    '''
    # get data
    event = event.get( "body", {} )
    event = json.loads( event )
    
    # return
    return event
    
def get_robo_balance( event, dynamo_db ):
    '''get robo balance data
    '''
    # define variable
    dbname = os.environ["env"]
    colname = "{}_{}".format( event.get("account_type"), os.environ["robo_table"] )
    
    # log 
    print( DEBUG_GET_ROBO_BAL.format( event.get("robo_type"), dbname, colname ) )

    # get and return data
    return dynamo_db.get( dbname, colname, event.get("robo_type") )

def get_user_balance( event, dynamo_db ):
    '''get user balance data
    '''
    # define variable
    dbname = os.environ["env"]
    colname = "{}_{}".format( event.get("account_type"), os.environ["user_table"] )
    _id = "{}_{}".format( event.get("user_id"), event.get("currency") ) 
    
    # log 
    print( DEBUG_GET_USER_BAL.format( _id, dbname, colname ) )

    # get and return data
    return dynamo_db.get( dbname, colname, _id )

def __is_float( amount ):
    '''check whether the 'amount' value is convertible to float
    '''
    try:
        float( amount )
        
    except ValueError:
        # return
        return "ValueError"
    
def __is_balance_sufficient( event, user_balance ):
    '''check whether user has sufficient balance to invest ( second checking, first was in fm1c )
    '''
    # check and return
    return True if float( user_balance.get("balance") ) >= float( event.get("amount") ) else False

def user2robo_wallet( event, user_balance, robo_balance ):
    '''transfer balance from user to robo wallet
    '''
    # checking
    if not __is_balance_sufficient( event, user_balance ):
        raise Exception( "Insufficient balance for investment, Currency: '{}'".format( event.get("currency") ) )
        
    elif __is_float( event.get("amount") ) != "ValueError" and \
       __is_float( robo_balance.get("wallet_balance") ) != "ValueError" and \
       __is_float( user_balance.get("balance") ) != "ValueError":
           
       # add and subtract balance
       robo_balance["wallet_balance"] = str( float( robo_balance.get("wallet_balance") ) + float( event.get("amount") ) )
       user_balance["balance"] = str( float( user_balance.get("balance") ) - float( event.get("amount") ) )
        
    else:
        raise Exception( "Unable to convert either deposit amount or user wallet bal or robo wallet bal to float" )

    # return
    return robo_balance, user_balance
    
def update_robo_balance( account_type, updated_robo_balance, dynamo_db ):
    '''update robo balance to db
    '''
    # define variable
    dbname = os.environ["env"]
    colname = "{}_{}".format( account_type, os.environ["robo_table"] )
    
    # log 
    print( DEBUG_UPDATE_ROBO_DYNAMODB.format( updated_robo_balance.get("_id"), dbname, colname, updated_robo_balance.get("wallet_balance") ) )
  
    # save
    res = dynamo_db.set( dbname, colname, updated_robo_balance )
    
    # check saving status
    if res["ResponseMetadata"].get("HTTPStatusCode") == 200:
        # log
        print( INFO_UPDATE_ROBO_SUCCESS_DYNAMODB.format( updated_robo_balance.get("_id"), dbname, colname, updated_robo_balance.get("wallet_balance") ) )
        
        # return
        return True
    
    else: 
        # log
        print( WARN_UPDATE_ROBO_FAIL_DYNAMODB.format( updated_robo_balance.get("_id"), dbname, colname, updated_robo_balance.get("wallet_balance") ) )
        
        # return
        return False
    
def update_user_balance( account_type, updated_user_balance, dynamo_db ):
    '''update user balance to db
    '''
    # define variable
    dbname = os.environ["env"]
    colname = "{}_{}".format( account_type, os.environ["user_table"] )
    
    # log 
    print( DEBUG_UPDATE_USER_DYNAMODB.format( updated_user_balance.get("_id"), dbname, colname, updated_user_balance.get("balance") ) )

    # save
    res = dynamo_db.set( dbname, colname, updated_user_balance )
    
    # check saving status
    if res["ResponseMetadata"].get("HTTPStatusCode") == 200:
        # log
        print( INFO_UPDATE_USER_SUCCESS_DYNAMODB.format( updated_user_balance.get("_id"), dbname, colname, updated_user_balance.get("balance") ) )
        
        # return
        return True
    
    else: 
        # log
        print( WARN_UPDATE_USER_FAIL_DYNAMODB.format( updated_user_balance.get("_id"), dbname, colname, updated_user_balance.get("balance") ) )
        
        # return
        return False
    
def update_req_order_status( event, new_status="rejected", error_msg="Unable to trigger step function, Step function: '{}'" ):
    '''update order status from sufficient to rejected - unable to trigger step function
    '''
    # define variable
    dbname = os.environ["env"]
    colname = "{}_{}".format( event.get("account_type"), os.environ["update_table"] )
    step_function_name = "{}_{}".format( os.environ["env"], os.environ["sf_name"] )
    
    # log 
    print( DEBUG_UPDATE_DYNAMODB.format( event.get("_id"), dbname, colname, new_status ) )

    # connect
    dynamo_db = Dynamodb_DataHandler()
    dynamo_db.connect()
    
    # get data
    data = dynamo_db.get( dbname, colname, event.get("_id") )
    
    # checking
    if data.get("status") == "sufficient":
        # update status
        data["status"] = new_status
        data["error_msg"] = error_msg.format( step_function_name )
        
    else:
        # raise exception
        raise Exception ( "The status is not 'sufficient', Status: '{}'".format( data.get("status") ) )
    
    # save
    update_res = dynamo_db.set( dbname, colname, data )

    # close connection
    dynamo_db.close()
    
    # checking
    if update_res["ResponseMetadata"].get("HTTPStatusCode") == 200:
        # log
        print( INFO_UPDATE_SUCCESS_DYNAMODB.format( data.get("_id"), dbname, colname, new_status ) )
    
    else: 
        # log
        print( WARN_UPDATE_FAIL_DYNAMODB.format( data.get("_id"), dbname, colname, new_status ) )

def trigger_step_function( event, new_event ):
    '''trigger step function
    '''
    # define variable
    step_function_name = "{}_{}".format( os.environ["env"], os.environ["sf_name"] )
    
    # log
    print( DEBUG_TRIGGER_SF.format( event.get("_id"), step_function_name ) )
    
    # connect
    client = boto3.client( "stepfunctions" )
    
    # trigger step function - update status
    res = client.start_execution(
        stateMachineArn = "arn:aws:states:us-east-2:581325732160:stateMachine:{}".format( step_function_name ),
        input= json.dumps( new_event )
    )
        
    # check trigger status
    if res["ResponseMetadata"].get("HTTPStatusCode") == 200:
        # log
        print( INFO_TRIGGER_SUCCESS_SF.format( event.get("_id"), step_function_name ) )
        
        # return
        return True
    
    else: 
        # log
        print( WARN_TRIGGER_FAIL_SF.format( event.get("_id"), step_function_name ) )
        
        # return
        return False

def accept_transaction( event ):
    # define variable
    sf_res = False
    step_function_name = "{}_{}".format( os.environ["env"], os.environ["sf_name"] )
    
    new_event = {
                    "statusCode": 200,
                    "body": json.dumps( {
                        "event": event,
                        "new_status": "transferred",
                    } )
                }
    
    # trigger step function - update status
    try:
        sf_res = trigger_step_function( event, new_event )
        
    except Exception as e:
        # log
        print( ERROR_TRIGGER_SF.format( event.get("_id"), step_function_name, str(e) ) )
        
        # update status to "reject"
        try:
            update_req_order_status( event )
        
        except Exception as e:
            # log 
            print( ERROR_UPDATE_DYNAMODB.format( str(e) ) )
            
        # return
        return {
                    "statusCode": 404,
                    "body": json.dumps( "Unable to trigger step function, _id: '{}'".format( event.get("_id") ) )
                }
    
    # return
    if sf_res:
        return {
                    "statusCode": 200,
                    "body": json.dumps( "Step function triggered success, _id: '{}'".format( event.get("_id") ) )
                }
    
    else: 
        return {
                    "statusCode": 404,
                    "body": json.dumps( "Step function triggered fail, _id: '{}'".format( event.get("_id") ) )
                }

def reject_transaction( event, error_message ):
    # define variable
    sf_res = False
    step_function_name = "{}_{}".format( os.environ["env"], os.environ["sf_name"] )
    
    new_event = {
                    "statusCode": 202,
                    "body": json.dumps( {
                        "event": event,
                        "new_status": "rejected",
                        "error_msg" : error_message
                    } )
                }
    
    # trigger step function - update status
    try:
        sf_res = trigger_step_function( event, new_event )
        
    except Exception as e:
        # log
        print( ERROR_TRIGGER_SF.format( event.get("_id"), step_function_name, str(e) ) )
        
        # update status to "reject"
        try:
            update_req_order_status( event )
        
        except Exception as e:
            # log 
            print( ERROR_UPDATE_DYNAMODB.format( str(e) ) )
        
        # return
        return {
                    "statusCode": 404,
                    "body": json.dumps( "Unable to trigger step function, _id: '{}'".format( event.get("_id") ) )
                }
    
    # return
    if sf_res:
        return {
                    "statusCode": 200,
                    "body": json.dumps( "Step function triggered success, _id: '{}'".format( event.get("_id") ) )
                }
    
    else: 
        return {
                    "statusCode": 404,
                    "body": json.dumps( "Step function triggered fail, _id: '{}'".format( event.get("_id") ) )
                }
    
    
def fm_init_deco( func ):
    def inner( event, context ):
        '''initialize
        '''
        # log
        print( DEBUG_START_LAMBDA )
        
        # checking
        if not isinstance( event, dict ):
            # log
            print( WARN_EVENT_NOT_DICT.format( type( event ) ) )
            
            # return
            return {
                        "statusCode": 404,
                        "body": json.dumps( "Event must be dictionary" )
                    }
        
        # return function
        return func( event, context )
    
    # return
    return inner

def fm_security_checking_deco( func ):
    def inner( event, context ):
        '''checking the input for:
        - verification
        '''
        # checking
        if __is_sqs( event ):
            # log 
            print( INFO_SQS_TRIGGERED )
            
            if not __is_valid_batch_size( event ):        
                # log
                print( WARN_BATCH_SIZE.format( len( event.get("Records") ) ) )
                
                # return
                return {
                            "statusCode": 404,
                            "body" : json.dumps( "Invalid batch size" )
                        }
            
            else:
                # get event
                try:
                    event = get_sqs_event( event )
                    
                except Exception as e:
                    # log
                    print( WARN_GET_EVENT.format( str(e) ) )
                    
                    # return
                    return {
                                "statusCode": 404,
                                "body": json.dumps( "Failed to get sqs event, Due to: '{}'".format( str(e) ) )
                            }
           
        elif __is_api_gw( event ):
            # log 
            print( INFO_API_TRIGGERED )
            
            # get event
            try:
                event = get_api_event( event )
                
            except Exception as e:
                # log
                print( WARN_GET_EVENT.format( str(e) ) )
                
                # return
                return {
                            "statusCode": 404,
                            "body": json.dumps( "Failed to get api event, Due to: '{}'".format( str(e) ) )
                        }
        
        # return function
        return func( event, context )
    
    # return
    return inner

def fm_data_checking_deco( func ):
    def inner( event, context ):
        '''checking the input for:
        - invalid data
        '''     
        # checking
        if not event:
            # log
            print( WARN_NO_EVENT )
            
            # return
            return {
                        "statusCode": 204,
                        "body" : json.dumps( "No event data found" )
                    }
    
        if not isinstance( event, dict ):
            # log
            print( WARN_EVENT_NOT_DICT.format( type( event ) ) )
            
            # return
            return {
                        "statusCode": 404,
                        "body" : json.dumps( "Event must be dictionary" )
                    }
            
        amount = event.get( "amount", "0" )
        if float( amount ) <= 0:
            # log
            print( WARN_ZERO_AMOUNT )
            
            # reject transaction
            return reject_transaction( event, "'amount' cannot be zero or negative" )
        
        # return function
        return func(event, context)
        
    # return
    return inner

@fm_init_deco
@fm_security_checking_deco
@fm_data_checking_deco
def main( event, context ):
    '''main
    sample input
    ------------
    # define event
    event = {
                  "user_id": "001",
                  "action": "deposit",
                  "currency": "USD",
                  "status": "sufficient",
                  "received_at": "2020-10-19 08:59:10.881638",
                  "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e",
                  "amount": "100",
                  "account_type": "demo",
                  "robo_type": "Robo_USD"
            }
    
    # sample sqs event with batch size 1 (not full version)
    event = {
                'Records': [{
                    'messageId': '5050fd45-327b-4923-ac48-d37388b63549', 
                    'receiptHandle': 'AQEBvTE3iiVC ... bLvN7R8JU1FK8AnS97xEmo=', 
                    'body': json.dumps( event ), 
                    'attributes': {'ApproximateReceiveCount': '1', 'SentTimestamp': '1602210795408', 'SenderId': 'AROAYOWNWXFAO6AW7MZIM:PentaipAwsFmDevelop-FmLam-Fm1aRequestReceiveFunction-1U34J434LIJN9', 'ApproximateFirstReceiveTimestamp': '1602210795409'}, 
                    'messageAttributes': {}, 
                    'md5OfBody': '850041fff21e3c8e1d86a8b940b63570', 
                    'eventSource': 'aws:sqs', 'eventSourceARN': 'arn:aws:sqs:us-east-2:581325732160:dev_demo_fm_pending_transfer_queue', 'awsRegion': 'us-east-2'
                    }]
                }
    
    # sample api event (not full version)
    event = {
                "httpMethod" : "POST",
                "body": json.dumps( event )
            }
    '''
    # connect
    dynamo_db = Dynamodb_DataHandler()
    dynamo_db.connect()
    
    # get robo balance
    robo_balance = get_robo_balance( event, dynamo_db )
    
    # checking
    if not robo_balance:
        # log
        print( WARN_NO_ROBO_RECORD.format( event.get("robo_type") ) )
        
        # close connection
        dynamo_db.close()
        
        # reject transaction
        return reject_transaction( event, "Robo has no existing wallet record in database, Robo: '{}'".format( event.get("robo_type") ) )
        
    # get user balance
    user_balance = get_user_balance( event, dynamo_db )
    
    # checking
    if not user_balance:
        # log
        print( WARN_NO_USER_RECORD.format( event.get("user_id") + "_" + event.get("currency") ) )
        
        # close connection
        dynamo_db.close()
        
        # reject transaction
        return reject_transaction( event, "User has no existing wallet record in database, User: '{}_{}'".format( event.get("user_id"), event.get("currency") ) )
            
    # define variable
    robo_res = False
    user_res = False
    updated_robo_balance = None
    updated_user_balance = None
    ori_robo_wallet_bal = robo_balance.get("wallet_balance")
    ori_user_wallet_bal = user_balance.get("balance")
    
    # transfer balance
    try:
        # CORE
        updated_robo_balance, updated_user_balance = user2robo_wallet( event, user_balance, robo_balance )

    except Exception as e:
        # log
        print( WARN_TRANSFER_BAL.format( str(e) ) )
        
        # close connection
        dynamo_db.close()
        
        # reject transaction
        return reject_transaction( event, "Unable to transfer balance from user to robo wallet, Due to: '{}'".format( str(e) ) )
    
    # checking
    if not updated_robo_balance or updated_robo_balance.get("wallet_balance") == ori_robo_wallet_bal or \
       not updated_user_balance or updated_user_balance.get("balance") == ori_user_wallet_bal:
        # log
        print( WARN_BAL_NOT_UPDATED.format( ori_robo_wallet_bal, updated_robo_balance.get("wallet_balance"), ori_user_wallet_bal, updated_user_balance.get("balance") ) )
        
        # close connection
        dynamo_db.close()
        
        # reject transaction
        return reject_transaction( event, "Robo/User balance not updated" )
            
    # save updated robo balance
    try:
        robo_res = update_robo_balance( event.get("account_type"), updated_robo_balance, dynamo_db )
    
    except Exception as e:
        # log
        print( ERROR_UPDATE_ROBO_DYNAMODB.format( str(e) ) )
    
    # save updated user balance
    if robo_res:
        try:
            user_res = update_user_balance( event.get("account_type"), updated_user_balance, dynamo_db )
    
        except Exception as e:
            # log
            print( ERROR_UPDATE_USER_DYNAMODB.format( str(e) ) )
        
    # close connection
    dynamo_db.close()
        
    # return
    if user_res:
        # accept transaction
         return accept_transaction( event )
        
    else:
        # reject transaction
        return reject_transaction( event, "Fail to update robo and/or user wallet, Robo _id: '{}', User _id: '{}'".format( updated_robo_balance.get("_id"), updated_user_balance.get("_id") ) )
        
    
# # test run 
# # TODO: comment this before push to aws
# if __name__ == "__main__":
#     # simulate aws environment
#     os.environ["env"] = "dev"
#     os.environ["user_table"] = "user_wallet"
#     os.environ["robo_table"] = "robo_wallet"
#     os.environ["sf_name"] = "fm_order_submit_state_machine"
#     os.environ["update_table"] = "fm_submit_req"
        
#     # define variable
#     event = {
#                   "user_id": "001",
#                   "action": "deposit",
#                   "currency": "USD",
#                   "status": "sufficient",
#                   "received_at": "2020-10-19 08:59:10.881638",
#                   "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e",
#                   "amount": "0",
#                   "account_type": "demo",
#                   "robo_type": "Robo_USD"
#             }
    
#     # sample sqs event with batch size 1 (not full version)
#     event = {
#                 'Records': [{
#                     'messageId': '5050fd45-327b-4923-ac48-d37388b63549', 
#                     'receiptHandle': 'AQEBvTE3iiVC ... bLvN7R8JU1FK8AnS97xEmo=', 
#                     'body': json.dumps( event ), 
#                     'attributes': {'ApproximateReceiveCount': '1', 'SentTimestamp': '1602210795408', 'SenderId': 'AROAYOWNWXFAO6AW7MZIM:PentaipAwsFmDevelop-FmLam-Fm1aRequestReceiveFunction-1U34J434LIJN9', 'ApproximateFirstReceiveTimestamp': '1602210795409'}, 
#                     'messageAttributes': {}, 
#                     'md5OfBody': '850041fff21e3c8e1d86a8b940b63570', 
#                     'eventSource': 'aws:sqs', 'eventSourceARN': 'arn:aws:sqs:us-east-2:581325732160:dev_demo_fm_pending_transfer_queue', 'awsRegion': 'us-east-2'
#                     }]
#                 }
    
#     # # sample sqs event with batch size 2 (not full version)
#     # event = {
#     #             'Records': [{
#     #                 'messageId': '5050fd45-327b-4923-ac48-d37388b63549', 
#     #                 'receiptHandle': 'AQEBvTE3iiVC ... bLvN7R8JU1FK8AnS97xEmo=', 
#     #                 'body': json.dumps( event ), 
#     #                 'attributes': {'ApproximateReceiveCount': '1', 'SentTimestamp': '1602210795408', 'SenderId': 'AROAYOWNWXFAO6AW7MZIM:PentaipAwsFmDevelop-FmLam-Fm1aRequestReceiveFunction-1U34J434LIJN9', 'ApproximateFirstReceiveTimestamp': '1602210795409'}, 
#     #                 'messageAttributes': {}, 
#     #                 'md5OfBody': '850041fff21e3c8e1d86a8b940b63570', 
#     #                 'eventSource': 'aws:sqs', 'eventSourceARN': 'arn:aws:sqs:us-east-2:581325732160:dev_demo_fm_pending_transfer_queue', 'awsRegion': 'us-east-2'
#     #                 },{
#     #                 'messageId': '5050fd45-327b-4923-ac48-d37388b63549', 
#     #                 'receiptHandle': 'AQEBvTE3iiVC ... bLvN7R8JU1FK8AnS97xEmo=', 
#     #                 'body': json.dumps( event ), 
#     #                 'attributes': {'ApproximateReceiveCount': '1', 'SentTimestamp': '1602210795408', 'SenderId': 'AROAYOWNWXFAO6AW7MZIM:PentaipAwsFmDevelop-FmLam-Fm1aRequestReceiveFunction-1U34J434LIJN9', 'ApproximateFirstReceiveTimestamp': '1602210795409'}, 
#     #                 'messageAttributes': {}, 
#     #                 'md5OfBody': '850041fff21e3c8e1d86a8b940b63570', 
#     #                 'eventSource': 'aws:sqs', 'eventSourceARN': 'arn:aws:sqs:us-east-2:581325732160:dev_demo_fm_pending_transfer_queue', 'awsRegion': 'us-east-2'
#     #                 }]
#     #             }
    
#     # # sample api event (not full version)
#     # event = {
#     #             "httpMethod" : "POST",
#     #             "body": json.dumps( event )
#     #         }
#     res = main( event, "" )