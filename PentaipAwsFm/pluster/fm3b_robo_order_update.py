"""
Des: 
    - this script will be triggered by state machine
    - this script will update the "status" of "{}_{}_fm_submit_req" table to either "confirmed" or "pending reverse"
    - if the new status is "confirmed", the state machine flow will continue to submit statement
    - if the new status is "pending reverse", the state machine flow will continue to refund script

Dependency: 
    - dynamodb -> { prod / stage / dev }_{ demo / live }_fm_submit_req 
    - environment variables of lambda:
        os.environ["table"] = "fm_submit_req"
        os.environ["env"] = "dev" # dev / stage / prod
        
Sample Input:
    # case #1 - confirmed
    {
         'statusCode': 200, 
         'body': '{
             "event": {
                         "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e", 
                         "user_id": "001", 
                         "robo_type": "Robo_USD", 
                         "amount": "100", 
                         "action": "deposit", 
                         "currency": "USD", 
                         "account_type": "demo", 
                         "received_at": "2020-10-19 08:59:10.881638", 
                         "status": "transferred"
                     }, 
             "new_status": "confirmed"
         }'
    }
    
    # case #2 - pending reverse
    {
         'statusCode': 202, 
         'body': '{
             "event": {
                         "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e", 
                         "user_id": "001", 
                         "robo_type": "Robo_USD", 
                         "amount": "1000000000", 
                         "action": "deposit", 
                         "currency": "USD", 
                         "account_type": "demo", 
                         "received_at": "2020-10-19 08:59:10.881638", 
                         "status": "transferred"
                     }, 
             "new_status": "pending reverse", 
             "error_msg": "Balance insufficient, Currency: \'USD\'"
         }'
    }
        
Sample Output ( passed to next lambda function through state machine ): 
     # case #1 - confirmed
     {
          'statusCode': 200, 
          'body': '{
              "event": {
                  "user_id": "001", 
                  "action": "deposit", 
                  "currency": "USD", 
                  "status": "confirmed", 
                  "received_at": "2020-10-19 08:59:10.881638", 
                  "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e", 
                  "amount": "100", 
                  "account_type": "demo", 
                  "robo_type": "Robo_USD"}, 
              "msg": "Successfully updated status, _id: \'a8098c1a-f86e-11da-bd1a-00112444be1e\', New status: \'transferred\'"
          }'
     }
    
    # case #2 - pending reverse
     {
          'statusCode': 203, 
          'body': '{
              "event": {
                  "user_id": "001", 
                  "action": "deposit", 
                  "currency": "USD", 
                  "status": "pending reverse", 
                  "received_at": "2020-10-19 08:59:10.881638", 
                  "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e", 
                  "amount": "100", 
                  "account_type": "demo", 
                  "robo_type": "Robo_USD",
                  "error_msg": "Fail to save to dynamodb and sqs, _id: 'a8098c1a-f86e-11da-bd1a-00112444be1e'"}, 
              "msg": "Successfully updated status, _id: 'a8098c1a-f86e-11da-bd1a-00112444be1e', New status: 'pending reverse'"
          }'
     }
    
Author:
    Tessy
    
Log:
    Tessy   20 Oct 2020     First version of this script
    
Status: 
    Alpha
"""

# import libs
import os
import json
try:
    from PentaipAwsFm.pluster.pluster_core.dynamodb_handler import DataHandler as Dynamodb_DataHandler
except:
    from pluster_core.dynamodb_handler import DataHandler as Dynamodb_DataHandler
  
# log messages
INFO_SF_TRIGGERED = "[INFO] Triggered by step function, File: 'Robo Order Update'"
INFO_UPDATE_SUCCESS_DYNAMODB = "[INFO] Updated success to dynamodb, File: 'Robo Order Update', _id: '{}', Table: '{}_{}', New status: '{}'"

DEBUG_START_LAMBDA = "[DEBUG] Start lambda, File: 'Robo Order Update'"
DEBUG_UPDATE_DYNAMODB = "[DEBUG] Updating to dynamodb, File: 'Robo Order Update', _id: '{}', Table: '{}_{}', New status: '{}'"

WARN_STATUS_CODE = "[WARNING] Invalid status code, File: 'Robo Order Update', Status code: '{}'"
WARN_GET_EVENT = "[WARNING] Failed to get event, File: 'Robo Order Update', Due to: '{}'"
WARN_NO_EVENT = "[WARNING] No event data found, File: 'Robo Order Update'"
WARN_EVENT_NOT_DICT = "[WARNING] Event is not dictionary, File: 'Robo Order Update', Datatype: '{}'"
WARN_INVALID_STATUS = "[WARNING] Invalid 'new_status' value, File: 'Robo Order Update', New status: '{}'"
WARN_UPDATE_FAIL_DYNAMODB = "[WARNING] Updated fail to dynamodb, File: 'Robo Order Update', _id: '{}', Table: '{}_{}', New status: '{}'"

ERROR_UPDATE_DYNAMODB = "[ERROR] Unable to update status to dynamodb, File: 'Robo Order Update', Due to: '{}'"
    
def __is_sf( event ):
    '''check whether event is triggered by step function
    '''
    # checking 
    if event and event.get("statusCode"):
        # return
        return True
        
    # return
    return False

def __is_valid_status_code( event ):
    '''check status code
    '''
    # checking
    return True if event.get("statusCode") in [ 200, 202 ] else False

def get_sf_event( event ):
    '''get event from body of sf
    '''
    # get data
    event = event.get( "body", {} )
    event = json.loads( event )
    
    # return
    return event
    
def update_req_order_status( event, new_status, error_msg ):
    '''update order status from received to confirmed / pending reverse
    '''
    # define variable
    dbname = os.environ["env"]
    colname = "{}_{}".format( event.get("account_type"), os.environ["table"] )
    
    # log 
    print( DEBUG_UPDATE_DYNAMODB.format( event.get("_id"), dbname, colname, new_status ) )

    # connect
    dynamo_db = Dynamodb_DataHandler()
    dynamo_db.connect()
    
    # get data
    data = dynamo_db.get( dbname, colname, event.get("_id") )
    
    # checking
    if data.get("status") == "transferred":
        # update status
        data["status"] = new_status
        
        if error_msg:
            data["error_msg"] = error_msg
        
    else:
        # raise exception
        raise Exception ( "The status is not 'transferred', Status: '{}'".format( data.get("status") ) )
    
    # save
    update_res = dynamo_db.set( dbname, colname, data )

    # close connection
    dynamo_db.close()
    
    # checking
    if update_res["ResponseMetadata"].get("HTTPStatusCode") == 200:
        # log
        print( INFO_UPDATE_SUCCESS_DYNAMODB.format( data.get("_id"), dbname, colname, new_status ) )
        
        # return
        return True, data
    
    else: 
        # log
        print( WARN_UPDATE_FAIL_DYNAMODB.format( data.get("_id"), dbname, colname, new_status ) )
        
        # return
        return False, {}

def update_accepted_req_order_status( event, new_status, error_msg ):
    '''update 'status' from 'transferred' to 'confirmed'
    '''
    # return
    return update_req_order_status( event, new_status, error_msg )

def update_rejected_req_order_status( event, new_status, error_msg ):
    '''update 'status' from 'transferred' to 'pending reverse'
    '''
    # return
    return update_req_order_status( event, new_status, error_msg )

def fm_init_deco( func ):
    def inner( event, context ):
        '''initialize
        '''
        # log
        print( DEBUG_START_LAMBDA )
        
        # checking
        if not isinstance( event, dict ):
            # log
            print( WARN_EVENT_NOT_DICT.format( type( event ) ) )
            
            # return
            return {
                        "statusCode": 404,
                        "body": json.dumps( "Event must be dictionary" )
                    }
        
        # return function
        return func( event, context )
    
    # return
    return inner

def fm_security_checking_deco( func ):
    def inner( event, context ):
        '''checking the input for:
        - verification
        '''
        if __is_sf( event ):
            # log
            print( INFO_SF_TRIGGERED )
            
            if not __is_valid_status_code( event ):        
                # log
                print( WARN_STATUS_CODE.format( event.get("statusCode") ) )
                
                # return
                return {
                            "statusCode": 404,
                            "body" : json.dumps( "Invalid status code" )
                        }
            
            else:
                # get event
                try:
                    event = get_sf_event( event )
                    
                except Exception as e:
                    # log
                    print( WARN_GET_EVENT.format( str(e) ) )
                    
                    # return
                    return {
                                "statusCode": 404,
                                "body": json.dumps( "Failed to get sf event, Due to: '{}'".format( str(e) ) )
                            }
                
        # return function
        return func( event, context )
    
    # return
    return inner

def fm_data_checking_deco( func ):
    def inner( event, context ):
        '''checking the input for:
        - invalid data
        '''
        # checking
        if not event:
            # log
            print( WARN_NO_EVENT )
            
            # return
            return {
                        "statusCode": 204,
                        "body" : json.dumps( "No event data found" )
                    }
    
        if not isinstance( event, dict ):
            # log
            print( WARN_EVENT_NOT_DICT.format( type( event ) ) )
            
            # return
            return {
                        "statusCode": 404,
                        "body" : json.dumps( "Event must be dictionary" )
                    }
        
        if event.get("new_status") and event.get("new_status") in [ "pending reverse", "confirmed" ]:
            # return function
            return func( event, context )
        
        else:
            # log
            print( WARN_INVALID_STATUS.format( event.get("new_status") ) )
            
            # return
            return {
                        "statusCode": 404,
                        "body" : json.dumps( "Invalid 'new_status' value, New status: '{}'".format( event.get("new_status") ) )
                    }
    
    # return
    return inner

@fm_init_deco
@fm_security_checking_deco
@fm_data_checking_deco
def main( event, context ):
    '''main
    sample input
    ------------
    # define event
    event = {
                  "user_id": "001",
                  "action": "deposit",
                  "currency": "USD",
                  "status": "transferred",
                  "received_at": "2020-10-19 08:59:10.881638",
                  "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e",
                  "amount": "100",
                  "account_type": "demo",
                  "robo_type": "Robo_USD"
            }
    
    # sample step function event ( status = pending reverse )
    event = {
                "statusCode": 202,
                "body": json.dumps( {
                    "event": event,
                    "new_status": "pending reverse",
                    "error_msg" :  "Fail to save to dynamodb and sqs, _id: '{}'".format( event.get("_id") )
                } )
            }
    
    # sample step function event ( status = transferred )
    event = {
                "statusCode": 200,
                "body": json.dumps( {
                    "event": event,
                    "new_status": "confirmed",
                } )
            }
    '''
    # define variable
    status_res = False
    event, new_status, error_msg = event.get("event"), event.get("new_status"), event.get("error_msg")
    
    # update status
    try:
        if new_status == "confirmed":
            status_res, event = update_accepted_req_order_status( event, new_status, error_msg )
            
        elif new_status == "pending reverse":
            status_res, event = update_rejected_req_order_status( event, new_status, error_msg )
    
    except Exception as e:
        # log 
        print( ERROR_UPDATE_DYNAMODB.format( str(e) ) )
        
        # return
        return {
                    "statusCode": 404,
                    "body": json.dumps( "Unable to update status, Due to: '{}'".format( str(e) ) )
                }
        
    # return
    if status_res and new_status == "confirmed":
        return {
                    "statusCode": 200,
                    "body": json.dumps( {
                                            "event": event,
                                            "msg": "Successfully updated status, _id: '{}', New status: '{}'".format( event.get("_id"), new_status )
                                        } )
                }
        
    elif status_res and new_status == "pending reverse":
        return {
                    "statusCode": 203,
                    "body": json.dumps( {
                                            "event": event,
                                            "msg":  "Successfully updated status, _id: '{}', New status: '{}'".format( event.get("_id"), new_status )
                                        } )
                }
        
    elif not status_res:
        return {
                    "statusCode": 404,
                    "body": json.dumps( "Fail to update status, _id: '{}'".format( event.get("_id") ) )
                }
        
    else:
        return {
                    "statusCode": 405,
                    "body": json.dumps( "Invalid 'status_res', _id: '{}', Status res: '{}'".format( event.get("_id"), status_res ) )
                }
    
   
# # test run 
# # TODO: comment this before push to aws
# if __name__ == "__main__":
#     # simulate aws environment
#     os.environ["env"] = "dev"
#     os.environ["table"] = "fm_submit_req"
        
#     # define variable
#     event = {
#                   "user_id": "001",
#                   "action": "deposit",
#                   "currency": "USD",
#                   "status": "transferred",
#                   "received_at": "2020-10-19 08:59:10.881638",
#                   "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e",
#                   "amount": "100",
#                   "account_type": "demo",
#                   "robo_type": "Robo_USD"
#             }
    
#     # # sample step function event ( status = pending reverse )
#     # event = {
#     #             "statusCode": 202,
#     #             "body": json.dumps( {
#     #                 "event": event,
#     #                 "new_status": "pending reverse",
#     #                 "error_msg" :  "Fail to save to dynamodb and sqs, _id: '{}'".format( event.get("_id") )
#     #             } )
#     #         }
    
#     # # sample step function event ( status = confirmed )
#     # event = {
#     #             "statusCode": 200,
#     #             "body": json.dumps( {
#     #                 "event": event,
#     #                 "new_status": "confirmed",
#     #             } )
#     #         }
    
#     res = main( event, "" )