"""
Des: 
    - this script will be triggered by state machine
    - this script is to save data to dynamodb and sqs
    - state machine will pass the output from this script to the next lambda funcion 

Dependency: 
    - dynamodb -> { prod / stage / dev }_{ demo / live }_user_robo_portfolio
    - sqs -> { prod / stage / dev }_{ demo / live }_robo_portfolio_queue 
    - sqs -> { prod / stage / dev }_{ demo / live }_user_robo_portfolio_queue 
    - environment variables of lambda:
        os.environ["user_table"] = "user_robo_portfolio"
        os.environ["user_queue"] = "user_robo_portfolio"
        os.environ["robo_queue"] = "robo_portfolio"
        os.environ["env"] = "dev" # dev / stage / prod
      
Sample Input:
    # event passed from step function
    {
          'statusCode': 200, 
          'body': '{
              "event": {
                  "user_id": "001", 
                  "action": "deposit", 
                  "currency": "USD", 
                  "status": "confirmed", 
                  "received_at": "2020-10-19 08:59:10.881638", 
                  "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e", 
                  "amount": "100", 
                  "account_type": "demo", 
                  "robo_type": "Robo_USD"}, 
              "msg": "Successfully updated status, _id: \'a8098c1a-f86e-11da-bd1a-00112444be1e\', New status: \'transferred\'"
          }'
     }
    
Sample Output (passed to next lambda function through step function): 
    # case #1 - accepted
    {
         'statusCode': 200, 
         'body': '{
             "event": {
                         "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e", 
                         "user_id": "001", 
                         "robo_type": "Robo_USD", 
                         "amount": "100", 
                         "action": "deposit", 
                         "currency": "USD", 
                         "account_type": "demo", 
                         "received_at": "2020-10-19 08:59:10.881638", 
                         "status": "confirmed",
                         "error_msg": "Robo/User balance not updated'"
                     }, 
             "new_status": "accepted"
         }'
    }
    
    # case #2 - rejected
    {
         'statusCode': 202, 
         'body': '{
             "event": {
                         "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e", 
                         "user_id": "001", 
                         "robo_type": "Robo_USD", 
                         "amount": "100", 
                         "action": "deposit", 
                         "currency": "USD", 
                         "account_type": "demo", 
                         "received_at": "2020-10-19 08:59:10.881638", 
                         "status": "confirmed",
                         "error_msg": "Robo/User balance not updated'"
                     }, 
             "new_status": "rejected", 
             "error_msg": "error_msg" : "Fail to save to dynamodb and/or sqs for either/both user and robo, _id: 'a8098c1a-f86e-11da-bd1a-00112444be1e'"
         }'
    }
    
Sample Data ( data/msg saved to dynamodb/sqs ):
    # data saved to user table
    {
         '_id': 'a8098c1a-f86e-11da-bd1a-00112444be1e', 
         'effective_date': '-1', 
         'unit': '-1', 
         'unit_price': '-1', 
         'total_cash': '-1', 
         'currency': 'USD'
     }
    
    # msg saved to user sqs
    {
         'user_id': '001', 
         'action': 'deposit', 
         'currency': 'USD', 
         'status': 'confirmed', 
         'received_at': '2020-10-19 08:59:10.881638', 
         '_id': 'a8098c1a-f86e-11da-bd1a-00112444be1e', 
         'amount': '100', 
         'account_type': 'demo', 
         'robo_type': 'Robo_USD', 
         'error_msg': "Robo/User balance not updated'"
     }
    
    # msg saved to robo sqs
    {
         '_id': 'Robo_USD', 
         'fiat_deposit': '100'
     }
    
Author:
    Tessy
    
Log:
    Tessy   18 Oct 2020     First version of this script
    Tessy   21 Oct 2020     Added some checking and made some amendments to the logging
    
Status: 
    Alpha
"""

# import libs
import os
import json
try:
    from PentaipAwsFm.pluster.pluster_core.sqs_handler import DataHandler as Sqs_DataHandler
    from PentaipAwsFm.pluster.pluster_core.dynamodb_handler import DataHandler as Dynamodb_DataHandler
except:
    from pluster_core.sqs_handler import DataHandler as Sqs_DataHandler
    from pluster_core.dynamodb_handler import DataHandler as Dynamodb_DataHandler
  
# log messages
INFO_SF_TRIGGERED = "[INFO] Triggered by step function, File: 'Statement Submit'"
INFO_SAVE_USER_SUCCESS_DYNAMODB = "[INFO] Saved success to dynamodb for user data, File: 'Statement Submit', _id: '{}', Table: '{}_{}'"
INFO_SAVE_USER_SUCCESS_SQS = "[INFO] Saved success to sqs for user data, File: 'Statement Submit', _id: '{}', Queue: '{}_queue'"
INFO_SAVE_ROBO_SUCCESS_SQS = "[INFO] Saved success to sqs for robo data, File: 'Statement Submit', _id: '{}', Queue: '{}_queue'"

DEBUG_START_LAMBDA = "[DEBUG] Start lambda, File: 'Statement Submit'"
DEBUG_SAVE_USER_DYNAMODB = "[DEBUG] Saving user data to dynamodb, File: 'Statement Submit', _id: '{}', Table: '{}_{}'"
DEBUG_SAVE_USER_SQS = "[DEBUG] Saving user data to sqs, File: 'Statement Submit', _id: '{}', Queue: '{}_queue'"
DEBUG_SAVE_ROBO_SQS = "[DEBUG] Saving robo data to sqs, File: 'Statement Submit', _id: '{}', Queue: '{}_queue'"

WARN_STATUS_CODE = "[WARNING] Invalid status code, File: 'Statement Submit', Status code: '{}'"
WARN_NO_EVENT = "[WARNING] No event data found, File: 'Statement Submit'"
WARN_GET_EVENT = "[WARNING] Failed to get event, File: 'Statement Submit', Due to: '{}'"
WARN_EVENT_NOT_DICT = "[WARNING] Event is not dictionary, File: 'Statement Submit', Datatype: '{}'"
WARN_SET_USER_DATA_STRUCTURE = "[WARNING] Unable to set user portfolio data structure, File: 'Statement Submit', Due to '{}'"
WARN_SET_ROBO_DATA_STRUCTURE = "[WARNING] Unable to set robo data structure, File: 'Statement Submit', Due to '{}'"
WARN_SAVE_USER_FAIL_DYNAMODB = "[WARNING] Saved fail to dynamodb for user data, File: 'Statement Submit', _id: '{}', Table: '{}_{}'"
WARN_SAVE_USER_FAIL_SQS = "[WARNING] Saved fail to sqs for user data, File: 'Statement Submit', _id: '{}', Queue: '{}_queue'"
WARN_SAVE_ROBO_FAIL_SQS = "[WARNING] Saved fail to sqs for robo data, File: 'Statement Submit', _id: '{}', Queue: '{}_queue'"

ERROR_SAVE_USER_DYNAMODB = "[ERROR] Unable to save user data to dynamodb, File: 'Statement Submit', Due to '{}'"
ERROR_SAVE_USER_SQS = "[ERROR] Unable to save user data to sqs, File: 'Statement Submit', Due to '{}'"
ERROR_SAVE_ROBO_SQS = "[ERROR] Unable to save robo data to sqs, File: 'Statement Submit', Due to '{}'"

def __is_sf( event ):
    '''check whether event is triggered by step function
    '''
    # checking 
    if event and event.get("statusCode"):
        # return
        return True
        
    # return
    return False

def __is_valid_status_code( event ):
    '''check status code
    '''
    # checking
    return True if event.get("statusCode") in [ 200 ] else False

def get_sf_event( event ):
    '''get event from body of sf
    '''
    # get data
    event = event.get( "body", {} )
    event = json.loads( event )
    
    # return
    return event.get( "event", {} )
    
def set_user_data_structure( event ):
    '''set user portfolio data structure
    '''
    # set structure
    data = {
                "_id": event.get("_id"),
                "effective_date": "-1",
                "unit": "-1",
                "unit_price": "-1",
                "total_cash": "-1",
                "currency": event.get("currency")
            }
    
    # return
    return data
    
def set_robo_data_structure( event ):
    '''set robo data structure
    '''
    # set structure
    data = {
                "_id": event.get("robo_type"),
                "fiat_deposit": event.get("amount")
            }
    
    # return
    return data

def user_save2dynamodb( event, user_data ):
    '''save to dynamodb
    '''
    # define variable
    dbname = os.environ["env"]
    colname = "{}_{}".format( event.get("account_type"), os.environ["user_table"] )
    
    # log 
    print( DEBUG_SAVE_USER_DYNAMODB.format( user_data.get("_id"), dbname, colname ) )
    
    # connect
    dynamo_db = Dynamodb_DataHandler()
    dynamo_db.connect()

    # save
    res = dynamo_db.set( dbname, colname, user_data )
    
    # close connection
    dynamo_db.close()

    # check saving status
    if res["ResponseMetadata"].get("HTTPStatusCode") == 200:
        # log
        print( INFO_SAVE_USER_SUCCESS_DYNAMODB.format( user_data.get("_id"), dbname, colname ) )
        
        # return
        return True
    
    else: 
        # log
        print( WARN_SAVE_USER_FAIL_DYNAMODB.format( user_data.get("_id"), dbname, colname ) )
        
        # return
        return False
        
def user_save2sqs( event, sqs_db ):
    '''save to sqs
    '''
    # define variable
    queue_name = "{}_{}_{}".format( os.environ["env"], event.get("account_type"), os.environ["user_queue"] )
    
    # log 
    print( DEBUG_SAVE_USER_SQS.format( event.get("_id"), queue_name ) )
    
    # save
    res = sqs_db.set( "{}_queue".format( queue_name ), "", event )

    # check saving status
    if res["ResponseMetadata"].get("HTTPStatusCode") == 200:
        # log
        print( INFO_SAVE_USER_SUCCESS_SQS.format( event.get("_id"), queue_name ) )
        
        # return
        return True
    
    else: 
        # log
        print( WARN_SAVE_USER_FAIL_SQS.format( event.get("_id"), queue_name ) )
        
        # return
        return False
        
def robo_save2sqs( event, robo_data, sqs_db ):
    '''save to sqs
    '''
    # define variable
    queue_name = "{}_{}_{}".format( os.environ["env"], event.get("account_type"), os.environ["robo_queue"] )
    
    # log 
    print( DEBUG_SAVE_ROBO_SQS.format( robo_data.get("_id"), queue_name ) )
    
    # save
    res = sqs_db.set( "{}_queue".format( queue_name ), "", robo_data )

    # check saving status
    if res["ResponseMetadata"].get("HTTPStatusCode") == 200:
        # log
        print( INFO_SAVE_ROBO_SUCCESS_SQS.format( robo_data.get("_id"), queue_name ) )
        
        # return
        return True
    
    else: 
        # log
        print( WARN_SAVE_ROBO_FAIL_SQS.format( robo_data.get("_id"), queue_name ) )
        
        # return
        return False

def fm_init_deco( func ):
    def inner( event, context ):
        '''initialize
        '''
        # log
        print( DEBUG_START_LAMBDA )
        
        # checking
        if not isinstance( event, dict ):
            # log
            print( WARN_EVENT_NOT_DICT.format( type( event ) ) )
            
            # return
            return {
                        "statusCode": 404,
                        "body": json.dumps( "Event must be dictionary" )
                    }
        
        # return function
        return func( event, context )
    
    # return
    return inner

def fm_security_checking_deco( func ):
    def inner( event, context ):
        '''checking the input for:
        - verification
        '''
        if __is_sf( event ):
            # log
            print( INFO_SF_TRIGGERED )
            
            if not __is_valid_status_code( event ):        
                # log
                print( WARN_STATUS_CODE.format( event.get("statusCode") ) )
                
                # return
                return {
                            "statusCode": 404,
                            "body" : json.dumps( "Invalid status code" )
                        }
            
            else:
                # get event
                try:
                    event = get_sf_event( event )
                    
                except Exception as e:
                    # log
                    print( WARN_GET_EVENT.format( str(e) ) )
                    
                    # return
                    return {
                                "statusCode": 404,
                                "body": json.dumps( "Failed to get sf event, Due to: '{}'".format( str(e) ) )
                            }
                
        # return function
        return func( event, context )
    
    # return
    return inner

def fm_data_checking_deco( func ):
    def inner( event, context ):
        '''checking the input for:
        - invalid data
        '''            
        # checking
        if not event:
            # log
            print( WARN_NO_EVENT )
            
            # return
            return {
                        "statusCode": 204,
                        "body": json.dumps( "No event data found" )
                    }
    
        if not isinstance( event, dict ):
            # log
            print( WARN_EVENT_NOT_DICT.format( type( event ) ) )
            
            # return
            return {
                        "statusCode": 404,
                        "body": json.dumps( "Event must be dictionary" )
                    }
      
        # return function
        return func( event, context )
        
    # return
    return inner

@fm_init_deco
@fm_security_checking_deco
@fm_data_checking_deco
def main( event, context ):
    '''main
    sample input
    ------------
    # define event
    event = {
                  "user_id": "001",
                  "action": "deposit",
                  "currency": "USD",
                  "status": "confirmed",
                  "received_at": "2020-10-19 08:59:10.881638",
                  "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e",
                  "amount": "100",
                  "account_type": "demo",
                  "robo_type": "Robo_USD",
                  "error_msg": "Robo/User balance not updated'"
            }
        
    # sample step function event
    event = {
                    "statusCode": 200,
                    "body": json.dumps( {
                        "event": event,
                        "msg":  "Successfully updated status, _id: '{}', New status: '{}'".format( event.get("_id"), event.get("status") )
                    } )
                }
    '''
    # define variable
    user_dynamo_res = False
    user_sqs_res = False
    robo_sqs_res = False
    
    # set user portfolio data structure
    try:
        user_data = set_user_data_structure( event )
    
    except Exception as e:
        # log 
        print( WARN_SET_USER_DATA_STRUCTURE.format( str(e) ) )

        # return
        return {
                    "statusCode": 404,
                    "body": json.dumps( "Unable to set user portfolio data structure, Due to'{}'".format( str(e) ) )
                }
    
    # save to dynamodb
    try:
        user_dynamo_res = user_save2dynamodb( event, user_data )
    
    except Exception as e:
        # log
        print( ERROR_SAVE_USER_DYNAMODB.format( str(e) ) )
    
    if user_dynamo_res:
        # connect
        sqs_db = Sqs_DataHandler()
        sqs_db.connect()
        
        # save to queue
        try:
            user_sqs_res = user_save2sqs( event, sqs_db )
    
        except Exception as e:
            # log
            print( ERROR_SAVE_USER_SQS.format( str(e) ) )
    
    if user_sqs_res:
        # set robo data structure
        try:
            robo_data = set_robo_data_structure( event )
        
        except Exception as e:
            # log 
            print( WARN_SET_ROBO_DATA_STRUCTURE.format( str(e) ) )
    
            # return
            return {
                        "statusCode": 404,
                        "body": json.dumps( "Unable to set robo data structure, Due to'{}'".format( str(e) ) )
                    }
        
        # save to queue
        try:
            robo_sqs_res = robo_save2sqs( event, robo_data, sqs_db )
    
        except Exception as e:
            # log
            print( ERROR_SAVE_ROBO_SQS.format( str(e) ) )
         
    # return
    if robo_sqs_res:
        if user_dynamo_res:
            # close connection
            sqs_db.close()

        # return
        return {
                    "statusCode": 200,
                    "body": json.dumps( {
                        "event": event,
                        "new_status": "accepted",
                    } )
                }
        
    else:
        if user_dynamo_res:
            # close connection
            sqs_db.close()

        # return
        return {
                    "statusCode": 202,
                    "body": json.dumps( {
                        "event": event,
                        "new_status": "rejected",
                        "error_msg" : "Fail to save to dynamodb and/or sqs for either/both user and robo, _id: '{}'".format( event.get("_id") )
                    } )
                }


# # test run 
# # TODO: comment this before push to aws
# if __name__ == "__main__":
#     # simulate aws environment
#     os.environ["env"] = "dev"
#     os.environ["user_table"] = "user_robo_portfolio"
#     os.environ["user_queue"] = "user_robo_portfolio"
#     os.environ["robo_queue"] = "robo_portfolio"
    
#     # define variable
#     event = {
#                   "user_id": "001",
#                   "action": "deposit",
#                   "currency": "USD",
#                   "status": "confirmed",
#                   "received_at": "2020-10-19 08:59:10.881638",
#                   "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e",
#                   "amount": "100",
#                   "account_type": "demo",
#                   "robo_type": "Robo_USD",
#                   "error_msg": "Robo/User balance not updated'"
#             }
        
#     # sample step function event
#     event = {
#                     "statusCode": 200,
#                     "body": json.dumps( {
#                         "event": event,
#                         "msg":  "Successfully updated status, _id: '{}', New status: '{}'".format( event.get("_id"), event.get("status") )
#                     } )
#                 }
    
#     res = main( event, "" )