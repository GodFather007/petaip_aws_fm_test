"""
Des: 
    - this script will be triggered by state machine
    - this script will save the data of those that fail to be refunded into dynamodb table
    - this script is the last task for the state machine

Dependency: 
    - dynamodb -> { prod / stage / dev }_{ demo / live }_error_pending_attention_withdrawal
    - environment variables of lambda:
        os.environ["table"] = "error_pending_attention_withdrawal"
        os.environ["env"] = "dev" # dev / stage / prod
        
Sample Input:
     {
          'statusCode': 203, 
          'body': '{
              "event": {
                  "refund_at": "2020-10-26 03:50:49.853940",
                  "user_id": "001", 
                  "action": "deposit", 
                  "currency": "USD", 
                  "status": "rejected", 
                  "received_at": "2020-10-19 08:59:10.881638", 
                  "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e", 
                  "amount": "100", 
                  "account_type": "demo",
                  "error_msg": "Robo/User balance not updated", "account_type": "demo", "robo_type": "Robo_USD"
                  "robo_type": "Robo_USD"}, 
              "msg": "Successfully updated status, _id: 'a8098c1a-f86e-11da-bd1a-00112444be1e', New status: 'rejected'"
          }'
     }
        
Sample Output: 
     {
          'statusCode': 200, 
          'body': '"Successfully saved to dynamodb, _id: 'a8098c1a-f86e-11da-bd1a-00112444be1e'"'
     }
    
Author:
    Tessy
    
Log:
    Tessy   18 Oct 2020     First version of this script
    Tessy   20 Oct 2020     Added some checking and made some amendments to the logging
    
Status: 
    Alpha
"""

# import libs
import os
import json
try:
    from PentaipAwsFm.pluster.pluster_core.dynamodb_handler import DataHandler as Dynamodb_DataHandler
except:
    from pluster_core.dynamodb_handler import DataHandler as Dynamodb_DataHandler
  
# log messages
INFO_SF_TRIGGERED = "[INFO] Triggered by step function, File: 'Robo Order Refund Fail'"
INFO_SAVE_SUCCESS_DYNAMODB = "[INFO] Saved success to dynamodb, File: 'Robo Order Refund Fail', _id: '{}', Table: '{}_{}'"

DEBUG_START_LAMBDA = "[DEBUG] Start lambda, File: 'Robo Order Refund Fail'"
DEBUG_SAVE_DYNAMODB = "[DEBUG] Saving to dynamodb, File: 'Robo Order Refund Fail', _id: '{}', Table: '{}_{}'"

WARN_NO_EVENT = "[WARNING] No event data found, File: 'Robo Order Refund Fail'"
WARN_GET_EVENT = "[WARNING] Failed to get event, File: 'Robo Order Refund Fail', Due to: '{}'"
WARN_STATUS_CODE = "[WARNING] Invalid status code, File: 'Robo Order Refund Fail', Status code: '{}'"
WARN_EVENT_NOT_DICT = "[WARNING] Event is not dictionary, File: 'Robo Order Refund Fail', Datatype: '{}'"
WARN_SAVE_FAIL_DYNAMODB = "[WARNING] Saved fail to dynamodb, File: 'Robo Order Refund Fail', _id: '{}', Table: '{}_{}'"

ERROR_SAVE_DYNAMODB = "[ERROR] Unable to save to dynamodb, File: 'Robo Order Refund Fail', Due to '{}'"

def __is_sf( event ):
    '''check whether event is triggered by step function
    '''
    # checking 
    if event and event.get("statusCode"):
        # return
        return True
        
    # return
    return False

def __is_valid_status_code( event ):
    '''check status code
    '''
    # checking
    return True if event.get("statusCode") in [ 203 ] else False

def get_sf_event( event ):
    '''get event from body of sf
    '''
    # get data
    event = event.get( "body", {} )
    event = json.loads( event )
    
    # return
    return event.get( "event", {} )

def save2dynamodb( event ):
    '''save to dynamodb
    '''
    # define variable
    dbname = os.environ["env"]
    colname = "{}_{}".format( event.get("account_type"), os.environ["table"] )
    
    # log 
    print( DEBUG_SAVE_DYNAMODB.format( event.get("_id"), dbname, colname ) )
    
    # connect
    dynamo_db = Dynamodb_DataHandler()
    dynamo_db.connect()

    # save
    res = dynamo_db.set( dbname, colname, event )
    
    # close connection
    dynamo_db.close()

    # check saving status
    if res["ResponseMetadata"].get("HTTPStatusCode") == 200:
        # log
        print( INFO_SAVE_SUCCESS_DYNAMODB.format( event.get("_id"), dbname, colname ) )
        
        # return
        return True
    
    else: 
        # log
        print( WARN_SAVE_FAIL_DYNAMODB.format( event.get("_id"), dbname, colname ) )
        
        # return
        return False
        
def fm_init_deco( func ):
    def inner( event, context ):
        '''initialize
        '''
        # log
        print( DEBUG_START_LAMBDA )
        
        # checking
        if not isinstance( event, dict ):
            # log
            print( WARN_EVENT_NOT_DICT.format( type( event ) ) )
            
            # return
            return {
                        "statusCode": 404,
                        "body": json.dumps( "Event must be dictionary" )
                    }
        
        # return function
        return func( event, context )
    
    # return
    return inner

def fm_security_checking_deco( func ):
    def inner( event, context ):
        '''checking the input for:
        - verification
        '''
        if __is_sf( event ):
            # log
            print( INFO_SF_TRIGGERED )
            
            if not __is_valid_status_code( event ):        
                # log
                print( WARN_STATUS_CODE.format( event.get("statusCode") ) )
                
                # return
                return {
                            "statusCode": 404,
                            "body" : json.dumps( "Invalid status code" )
                        }
            
            else:
                # get event
                try:
                    event = get_sf_event( event )
                    
                except Exception as e:
                    # log
                    print( WARN_GET_EVENT.format( str(e) ) )
                    
                    # return
                    return {
                                "statusCode": 404,
                                "body": json.dumps( "Failed to get sf event, Due to: '{}'".format( str(e) ) )
                            }
                
        # return function
        return func( event, context )
    
    # return
    return inner

def fm_data_checking_deco( func ):
    def inner( event, context ):
        '''checking the input for:
        - invalid data
        '''        
        # checking
        if not event:
            # log
            print( WARN_NO_EVENT )
            
            # return
            return {
                        "statusCode": 204,
                        "body": json.dumps( "No event data found" )
                    }
    
        if not isinstance( event, dict ):
            # log
            print( WARN_EVENT_NOT_DICT.format( type( event ) ) )
            
            # return
            return {
                        "statusCode": 404,
                        "body": json.dumps( "Event must be dictionary" )
                    }
      
        # return function
        return func( event, context )
        
    # return
    return inner

@fm_init_deco
@fm_security_checking_deco
@fm_data_checking_deco
def main( event, context ):
    '''main
    sample input
    ------------
    # define event
    event = {
                  "user_id": "001",
                  "action": "deposit",
                  "currency": "USD",
                  "status": "rejected",
                  "received_at": "2020-10-19 08:59:10.881638",
                  "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e",
                  "amount": "100",
                  "account_type": "demo",
                  "robo_type": "Robo_USD",
                  "error_msg": "Robo/User balance not updated"
            }
        
    # sample step function event
    event = {
                "statusCode": 203,
                "body": json.dumps( {
                    "event": event,
                    "msg":  "Successfully updated status, _id: '{}', New status: '{}'".format( event.get("_id"), event.get("status") )
                } )
            }
    '''
    # define variable
    dynamo_res = False
    
    # save to dynamodb
    try:
        dynamo_res = save2dynamodb( event )
    
    except Exception as e:
        # log
        print( ERROR_SAVE_DYNAMODB.format( str(e) ) )
    
    # return
    if dynamo_res:
        return {
                    "statusCode": 200,
                    "body": json.dumps( "Successfully saved to dynamodb, _id: '{}'".format( event.get("_id") ) )
                }
        
    else:
        return {
                    "statusCode": 404,
                    "body": json.dumps( "Fail to save to dynamodb, _id: '{}'".format( event.get("_id") ) )
                }
        
    
# # test run 
# # TODO: comment this before push to aws
# if __name__ == "__main__":
#     # simulate aws environment
#     os.environ["env"] = "dev"
#     os.environ["table"] = "error_pending_attention_withdrawal"
    
#     # define variable
#     event = {
#                   "user_id": "001",
#                   "action": "deposit",
#                   "currency": "USD",
#                   "status": "rejected",
#                   "received_at": "2020-10-19 08:59:10.881638",
#                   "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e",
#                   "amount": "100",
#                   "account_type": "demo",
#                   "robo_type": "Robo_USD",
#                   "error_msg": "Robo/User balance not updated"
#             }
        
#     # sample step function event
#     event = {
#                 "statusCode": 203,
#                 "body": json.dumps( {
#                     "event": event,
#                     "msg":  "Successfully updated status, _id: '{}', New status: '{}'".format( event.get("_id"), event.get("status") )
#                 } )
#             }
        
#     res = main( event, "" )