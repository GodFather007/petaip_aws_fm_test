#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 22 10:54:49 2020

Des: this script is to create sample data for user wallet and robo wallet table in dynamodb

@author: tessy
"""
# import libs
import os
import random
try:
    from PentaipAwsFm.pluster.pluster_core.dynamodb_handler import DataHandler as Dynamodb_DataHandler
except:
    from pluster_core.dynamodb_handler import DataHandler as Dynamodb_DataHandler
    
# define variable
currency_list = [
                        "HUF", #HungarianForint
                        "INR", #IndianRupee
                        "TWD", #NewTaiwanDollar
                        "GBP", #Poundsterling
                        "PLN", #PolishZłoty
                        "JPY", #JapaneseYen
                        "SEK", #SwedishKrona
                        "THB", #ThaiBaht
                        "EUR", #Euro
                        "NOK", #NorwegianKrone
                        "MYR", #MalaysianRinggit
                        "CHF", #SwissFranc
                        "DKK", #DanishKrone
                        "CNY", #Renminbi
                        "IDR", #IndonesianRupiah
                        "SAR", #SaudiRiyal
                        "CAD", #CanadianDollar
                        "ISK", #IcelandicKróna
                        "PKR", #PakistaniRupee
                        "PHP", #PhilippinePeso
                        "TRY", #TurkishLira
                        "KRW", #SouthKoreanWon
                        "MXN", #MexicanPeso
                        "ILS", #IsraeliNewShekel
                        "ZAR", #SouthAfricanRand
                        "BRL", #BrazilianReal
                        "VND", #VietnameseDong
                        "GBX", #PenceSterling
                        "USD", #UnitedStatesDollar
                        "CLP", #ChileanPeso
                        "HKD", #HongKongDollar
                        "SGD", #SingaporeDollar
                    ]
user_id_list = [ 
                    "001", "002", "003", "004", "005", "006", "007", "008", "009", "010", "011", "012", "013", "014", "015", \
                    "016", "017", "018", "019", "020", "021", "022", "023", "024", "025", "026", "027", "028", "029", "030"
                ]
    
def save2robo_wallet( currency, dynamo_db ):
    '''save to robo wallet 
    '''
    # define variable
    dbname = os.environ["env"]
    colname = "{}".format( os.environ["robo_table"] )
    
    data = {
                "_id": "Robo_{}".format( currency ),
                "wallet_balance": "0", #str(random.randint(0, 9999999)),
            }
    
    # save
    res = dynamo_db.set( dbname, colname, data )

    # check saving status
    if res["ResponseMetadata"].get("HTTPStatusCode") == 200:
        # log
        print( "Saved success robo wallet, _id: '{}'".format( data.get("_id") ) )
        
        # return
        return True
    
    else: 
        # log
        print( "Saved fail robo wallet, _id: '{}'".format( data.get("_id") ) )
        
        # return
        return False
    
def save2user_wallet( user_id, currency, dynamo_db ):
    '''save to user wallet 
    '''
    # define variable
    dbname = os.environ["env"]
    colname = "{}".format( os.environ["user_table"] )
    
    data = {
                "_id": "{}_{}".format( user_id, currency ),
                "balance": str(random.randint(30000, 100000)),
            }
    
    # save
    res = dynamo_db.set( dbname, colname, data )

    # check saving status
    if res["ResponseMetadata"].get("HTTPStatusCode") == 200:
        # log
        print( "Saved success user wallet, _id: '{}'".format( data.get("_id") ) )
        
        # return
        return True
    
    else: 
        # log
        print( "Saved fail user wallet, _id: '{}'".format( data.get("_id") ) )
        
        # return
        return False
    
def main():
    '''main
    '''
    # define variable
    robo_res = False
    user_res = False
    
    # connect
    dynamo_db = Dynamodb_DataHandler()
    dynamo_db.connect()

    # create and save data
    for currency in currency_list:
        robo_res = save2robo_wallet( currency, dynamo_db )
        
        for user_id in user_id_list:
            user_res = save2user_wallet( user_id, currency, dynamo_db )
        
    # print
    print( "Robo res: '{}', User res: '{}'".format( robo_res, user_res ) )

    # close connection
    dynamo_db.close()


# if __name__ == "__main__":
#     # define variable
#     os.environ["env"] = "dev"
#     os.environ["user_table"] = "demo_user_wallet"
#     os.environ["robo_table"] = "demo_robo_wallet"
    
#     res = main()