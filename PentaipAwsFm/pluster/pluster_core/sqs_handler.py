# -*- coding: utf-8 -*-
"""
DES: 
    Connect to SQS in AWS to execute cache query
USE CASE: 
    AWS lambda
DEPENDENCY: 
    python 3.7
    boto3
    SQS have exists queue
    AWS POLICY PERMISSION:
        *sqs* related
ENVIRONMENT: 
    AWS lambda (layer)
AUTHUR: 
    Yeong
CREATED:
    2020 July 19th
REMARK:
    "_hd" (handler)
    "_db" (database)
    "_tb" (table)
    "_id" (key)
    contains this 3 information so we know where to trace back the data
LOG: 
    Yeong, 2020-07-19, first version
LICENSE:
    Free license for Finnotech to use (GreenPro Capital)
"""

import boto3
import json

""" NOT APPLICABLE
# parameter
HOST = "localhost"
PORT = 27017
# local parameter
try:
    from local_settings import *
except:
    pass
"""

class DataHandlerBase():
    """
    DES: Prepare the base class and alias of the method
    AUTHOR: Yeong
    LOG: Yeong, 2020-07-19, create this function (method are moving from data_handler class)
    """
    _db = None
    _client = None
        
    def client(self, *args, **kwargs):
        """
        DES: return client connection
        AUTHUR: Yeong
        INPUT: None
        OUTPUT: client object
        EXCEPTION: client object are None
        LOG: Yeong, 2020-07-19, first version
        """
        
        assert self._client
        return self._client
    
    def db(self, *args, **kwargs):
        """
        DES: return database or table connection
        AUTHUR: Yeong
        INPUT: None
        OUTPUT: client object
        EXCEPTION: db / table object are None
        LOG: Yeong, 2020-07-19, first version
        """
        
        assert self._db
        return self._db
    
    # alias of connect()
    def connect_db(self):
        self.connect()
        
    # alias of connect()
    def conn(self):
        self.connect()
    
    # alias of client()
    def get_client(self):
        return self.client()
    
    # alias of db()
    def table(self, *args, **kwargs):
        return self.db()
    
    # alias of db()
    def get_table(self, *args, **kwargs):
        return self.db()
    
    # alias of db()
    def get_db(self, *args, **kwargs):
        return self.db()
        
    # alias of delete()
    def remove(self, *args, **kwargs):
        return self.delete(*args, **kwargs)
        
    # alias of close()
    def close_conn(self):
        self.close()
        
    # alias of close()
    def close_connection(self):
        self.close()
        
class DataHandlerClient(DataHandlerBase):
    """
    DES: Prepare the base class and alias of the method
    AUTHOR: Yeong
    LOG: Yeong, 2020-07-19, create this function (method are moving from data_handler class)
    """
    
    def connect(self, *args, **kwargs):
        """
        DES: establish the connection
        AUTHUR: Yeong
        INPUT: None
        OUTPUT: client object
        LOG: Yeong, 2020-07-19, first version
        """
        
        # checking
        if self._client:
            return self._client
        
        # connect
        self._client = boto3.client("sqs")
        
        # return data
        return self._client
        
    def close(self, *args, **kwargs):
        """
        DES: close the connection
        AUTHUR: Yeong
        INPUT: None
        OUTPUT: boolean
        LOG: Yeong, 2020-07-19, first version
        REMARK: This one do nothing in this script
        """

        # close connection
        try: 
            self._client.close()
        except: 
            pass
        
        # clear cache
        self._client = None

class DataHandlerOperation(DataHandlerClient):
    """
    DES: Prepare the base class and alias of the method
    AUTHOR: Yeong
    LOG: Yeong, 2020-07-19, create this function (method are moving from data_handler class)
    """
    
    def set(self, *args, **kwargs):
        """
        DES: upsert a data
        AUTHUR: Yeong
        INPUT: string, string, dictionary
        USE CASE:
            output = db.set("queuename", "", {"_id":str(var), "value":"testing"})
        OUTPUT: boolean
        EXCEPTION: 
            ?
        LOG: Yeong, 2020-07-19, first version
        """
        
        # make sure the data have 3 element
        assert self._client
        dbname, colname, value = args
        
        # checking key
        assert isinstance(value, dict)
        
        # put data
        queue = self._client.get_queue_url(QueueName=dbname)
        data = self._client.send_message(QueueUrl=queue["QueueUrl"], MessageBody=json.dumps(value))
        
        # return data
        return data
    
    def get(self, *args, **kwargs):
        """
        DES: get a data
        AUTHUR: Yeong
        INPUT: string, string, string
        USE CASE:
            output = db.set("test", "", "")
        OUTPUT: dictionary
        EXCEPTION: 
            ?
        ERROR EXCEPT:
            ?
        LOG: Yeong, 2020-07-19, first version
        """
        
        # make sure the data have 3 element
        assert self._client
        dbname, colname, keyname = args
        
        # get data
        try:
            queue = self._client.get_queue_url(QueueName=dbname)
            output = self._client.receive_message(QueueUrl=queue["QueueUrl"])
            data = json.loads(output["Messages"][0]["Body"])
            self._client.delete_message(QueueUrl=queue["QueueUrl"], ReceiptHandle=output["Messages"][0]["ReceiptHandle"])
        except:
            data = {}
        
        # return data
        return data
        
    def delete(self, *args, **kwargs):
        """
        DES: delete a data
        AUTHUR: Yeong
        INPUT: string, string, string
        USE CASE:
            output = db.set("test", "", "")
        OUTPUT: boolean
        LOG: Yeong, 2020-07-19, first version, reuse get()
        """
        
        # get and return data
        if not self.get(*args):
            return False
        return True

class DataHandler(DataHandlerOperation):
    """connect to data source
        API standard is refer to redis API method
        not fully copy but basically the same
    AUTHUR: Yeong
    LOG: Yeong, 2020-07-19, first version
    """
                    
    def sets(self, *args, **kwargs):
        """
        DES: upsert a list of data to SQS queue
        AUTHUR: Yeong
        INPUT: string, string, list of dictionary
        USE CASE:
            output = db.set("test", "", [{ "_id":str(i), "value":"testing" } for i in range(100)])
        OUTPUT: list of boolean
        EXCEPTION: 
            ?
        LOG: Yeong, 2020-07-19, first version
        """
        
        # make sure the data have 3 element
        assert self._client
        
        # checking
        dbname, colname, values = args
        
        # connection
        queue = self._client.get_queue_url(QueueName=dbname)
        output = self._client.send_message_batch(
            QueueUrl = queue["QueueUrl"],
            Entries = [ { "Id":str(i), "MessageBody":json.dumps(v) } for i, v in enumerate(values) ]
        )
        
        # return
        return [True] * len(values)
                
    def gets(self, *args, **kwargs):
        """
        DES: get a list of data
        AUTHUR: Yeong
        INPUT: string, string, integer
        USE CASE:
            # output = db.gets("test", "", 10)
        OUTPUT: list of dictionary
        EXCEPTION: 
            ?
        LOG: Yeong, 2020-07-19, first version
        REMARKS: dynamodb every time query will expand the cache size so next time faster
        """
        
        # make sure the data have 3 element
        assert self._client
        dbname, colname, max_message = args
        
        # error checking
        try:
            max_message = abs(int(max_message))
            if not max_message:
                max_message = 1
        except: 
            max_message = 1
        
        # connection
        queue = self._client.get_queue_url(QueueName=dbname)
        output = self._client.receive_message(
            QueueUrl = queue["QueueUrl"],
            MaxNumberOfMessages = max_message if max_message < 11 else 10
        )
            
        # return
        if "Messages" in output:
            data =  [ json.loads( i["Body"] ) for i in output["Messages"] ]
            self._client.delete_message_batch(
                QueueUrl=queue["QueueUrl"],
                Entries=[ {"Id": str(i), "ReceiptHandle":v["ReceiptHandle"]} for i, v in enumerate(output["Messages"]) ]
            )
            return data
        else:
            return []
                    
    def dels(self, *args, **kwargs):
        """
        DES: delete a list of data
        AUTHUR: Yeong
        INPUT: string, string, integer
        USE CASE:
            output = db.dels("test","", 10)
        OUTPUT: list of boolean
        LOG: Yeong, 2020-07-19, first version
        """
        
        return [True] * len(self.gets(*args, **kwargs))
    
    def pop(self, *args, **kwargs):
        """
        DES: find and delete a data
        AUTHUR: Yeong
        INPUT: string, string, string
        USE CASE:
            output = db.pop("test", "", "")
        OUTPUT: dictionary
        LOG: Yeong, 2020-07-19, first version
        """
        
        return self.get(*args, **kwargs)
                
    def pops(self, *args, **kwargs):
        """
        DES: find and delete a list of data
        AUTHUR: Yeong
        INPUT: string, string, integer
        USE CASE:
            output = db.pops("test", "", 10)
        OUTPUT: list of dictionary
        LOG: Yeong, 2020-07-19, first version
        """
        
        return self.gets(*args, **kwargs)
    
    # def keys(self, *args, **kwargs):
    #     """
    #     DES: find a list of keys
    #     AUTHUR: Yeong
    #     INPUT: string, string, int
    #     USE CASE:
    #         output = db.dels("test","lambda", 10)
    #         where table name = test_lambda
    #     OUTPUT: list
    #     LOG: Yeong, 2020-07-05, first version
    #     LOG: MH, 2020-07-06, do testing
    #     REMARKS: get latest record first
    #              not support nagative value
    #     """
        
    #     # make sure the data have 3 element
    #     assert self._client
    #     dbname, colname, size = args
        
    #     # get data
    #     data = [ i["_id"] for i in self._client.Table(dbname+"_"+colname).scan(Limit=int(size))["Items"] ]
    #     # data = self._client.Table(dbname+"_"+colname).scan(Limit=int(size))["Items"]
        
    #     # return data
    #     return data
        
    def find_one(self, *args, **kwargs):
        """
        DES: find latest item, but in SQS, is find the first item if is FIFO
            or find the first item in standard SQS but no guaranteee.
        AUTHUR: Yeong
        INPUT: string, string
        USE CASE:
            output = db.find_one("test","")
        OUTPUT: dict
        LOG: Yeong, 2020-07-19, first version
        """
        
        # make sure the data have 3 element
        assert self._client
        dbname, colname = args
        
        # get data
        # return data
        try:
            return self.get(dbname, colname, "")
        except:
            return {}
        
    def get_one(self, *args, **kwargs):
        """
        DES: find latest item, but in SQS, is find the first item if is FIFO
            or find the first item in standard SQS but no guaranteee.
        AUTHUR: Yeong
        INPUT: string, string
        USE CASE:
            output = db.find_one("test","")
        OUTPUT: dict
        LOG: Yeong, 2020-07-19, first version
        """
        
        return self.find_one(*args, **kwargs)

