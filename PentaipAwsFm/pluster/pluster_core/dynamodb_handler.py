# -*- coding: utf-8 -*-
"""
DES: 
    Connect to dynamodb in AWS to execute database query
USE CASE: 
    AWS lambda
DEPENDENCY: 
    python 3.7
    boto3
    dynamodb have exists table
    AWS POLICY PERMISSION:
        *dynamodb* related
ENVIRONMENT: 
    AWS lambda (layer)
AUTHUR: 
    Yeong
CREATED:
    2020 July 5th
REMARK:
    "_id" (must) is the table primary key
LOG: 
    Yeong, 2020-07-05, first version
    MH, 2020-07-06, do testing
    Yeong, 2020-07-08, move some function to another class for clean reading
"""

import boto3
from boto3.dynamodb.conditions import Attr
""" NOT APPLICABLE
# parameter
HOST = "localhost"
PORT = 27017
# local parameter
try:
    from local_settings import *
except:
    pass
"""

class DataHandlerBase():
    """
    DES: Prepare the base class and alias of the method
    AUTHOR: Yeong
    LOG: Yeong, 2020-07-08, create this function (method are moving from data_handler class)
    """
    _db = None
    _client = None
        
    def client(self, *args, **kwargs):
        """
        DES: return client connection
        AUTHUR: Yeong
        INPUT: None
        OUTPUT: client object
        EXCEPTION: client object are None
        LOG: Yeong, 2020-07-05, first version
        """
        
        assert self._client
        return self._client
    
    def db(self, *args, **kwargs):
        """
        DES: return database or table connection
        AUTHUR: Yeong
        INPUT: None
        OUTPUT: client object
        EXCEPTION: db / table object are None
        LOG: Yeong, 2020-07-05, first version
        """
        
        assert self._db
        return self._db
    
    # alias of connect()
    def connect_db(self):
        self.connect()
        
    # alias of connect()
    def conn(self):
        self.connect()
    
    # alias of client()
    def get_client(self):
        return self.client()
    
    # alias of db()
    def table(self, *args, **kwargs):
        return self.db()
    
    # alias of db()
    def get_table(self, *args, **kwargs):
        return self.db()
    
    # alias of db()
    def get_db(self, *args, **kwargs):
        return self.db()
        
    # alias of delete()
    def remove(self, *args, **kwargs):
        return self.delete(*args, **kwargs)
        
    # alias of close()
    def close_conn(self):
        self.close()
        
    # alias of close()
    def close_connection(self):
        self.close()
        
class DataHandlerClient(DataHandlerBase):
    """
    DES: Prepare the base class and alias of the method
    AUTHOR: Yeong
    LOG: Yeong, 2020-07-08, create this function (method are moving from data_handler class)
    """
    
    def connect(self, *args, **kwargs):
        """
        DES: establish the connection
        AUTHUR: Yeong
        INPUT: None
        OUTPUT: client object
        LOG: Yeong, 2020-07-05, first version
        """
        
        # checking
        if self._client:
            return self._client
        
        # connect
        self._client = boto3.resource("dynamodb")
        
        # return data
        return self._client
        
    def close(self, *args, **kwargs):
        """
        DES: close the connection
        AUTHUR: Yeong
        INPUT: None
        OUTPUT: boolean
        LOG: Yeong, 2020-07-05, first version
        REMARK: This one do nothing in this script
        """

        # close connection
        try: 
            self._client.close()
        except: 
            pass
        
        # clear cache
        self._client = None

class DataHandlerOperation(DataHandlerClient):
    """
    DES: Prepare the base class and alias of the method
    AUTHOR: Yeong
    LOG: Yeong, 2020-07-08, create this function (method are moving from data_handler class)
    """
    
    def set(self, *args, **kwargs):
        """
        DES: upsert a data
        AUTHUR: Yeong
        INPUT: string, string, dictionary with "_id" as a key
        USE CASE:
            output = db.set("test", "lambda", {("_id":str(var), "value":"testing")})
            where table name = test_lambda
        OUTPUT: boolean
        EXCEPTION: 
            1. client object are None, not connect to database.
            2. missing "_id"
            3. "_id" (must) is a primary key in dynamodb's table
            4. fail to upsert item
            5. input not 3 item
            5.1. 3rd item must be a dictionary with "_id" key and value not None
        LOG: Yeong, 2020-07-05, first version
            MH, 2020-07-06, do testing
        """
        
        # make sure the data have 3 element
        assert self._client
        dbname, colname, value = args
        
        # checking key
        assert value.get("_id")
        
        # put data
        data = self._client.Table(dbname+"_"+colname).put_item(Item=value)
        
        # return data
        return data
    
    def get(self, *args, **kwargs):
        """
        DES: get a data
        AUTHUR: Yeong
        INPUT: string, string, string
        USE CASE:
            output = db.set("test", "lambda", str(key))
            where table name = test_lambda
        OUTPUT: dictionary
        EXCEPTION: 
            1. client object are None, not connect to database.
            2. "_id" (must) is a primary key in dynamodb's table
            3. input not 3 item
        ERROR EXCEPT:
            ResourceNotFoundException
        LOG: Yeong, 2020-07-05, first version
            MH, 2020-07-06, do testing
        """
        
        # make sure the data have 3 element
        assert self._client
        dbname, colname, keyname = args
        
        # get data
        try:
            data = self._client.Table(dbname+"_"+colname).get_item(
                Key={"_id": keyname})["Item"]
        except:
            data = {}
        
        # return data
        return data
        
    def delete(self, *args, **kwargs):
        """
        DES: delete a data
        AUTHUR: Yeong
        INPUT: string, string, string
        USE CASE:
            output = db.set("test", "lambda", str(key))
            where table name = test_lambda
        OUTPUT: boolean
        LOG: Yeong, 2020-07-05, first version
            MH, 2020-07-06, do testing
        """

        # make sure the data have 3 element
        assert self._client
        dbname, colname, keyname = args
        
        # get and return data
        try:
            self._client.Table(dbname+"_"+colname).delete_item(
                Key={"_id": keyname})
            return True
        except:
            return False

class DataHandler(DataHandlerOperation):
    """connect to data source
        API standard is refer to redis API method
        not fully copy but basically the same
    AUTHUR: Yeong
    LOG: Yeong, 2020-07-05, first version
    """
        
    def __sets(self, *args, **kwargs):
        
        # checking
        dbname, colname, values = args
        
        # connection
        table = self._client.Table(dbname+"_"+colname)
        
        # batch upsert
        with table.batch_writer() as batch:
            for value in values:
                try:
                    # checking key
                    assert value.get("_id")
                    batch.put_item(Item=value)
                    # yield success
                    yield True
                except:
                    # yield fail
                    yield False
                    
    def sets(self, *args, **kwargs):
        """
        DES: set a list of data
        AUTHUR: Yeong
        INPUT: string, string, list of dictionary
        USE CASE:
            output = db.set("test", "lambda", [{ "_id":str(i), "value":"testing" } for i in range(100)])
            where table name = test_lambda
        OUTPUT: list of boolean
        EXCEPTION: 
            1. client object are None, not connect to database.
            2. input not 3 item
        LOG: Yeong, 2020-07-05, first version
            MH, 2020-07-06, do testing
        REMARK: dynamodb every new insert will expand the cache size so next time faster.
        """
        
        # make sure the data have 3 element
        assert self._client
        
        # batch upsert & return
        return list(self.__sets(*args, **kwargs))
    
    def __gets(self, dbname, colname, keynames):
        
        # get and yield data
        for keyname in keynames:
            try:
                yield self._client.Table(dbname+"_"+colname).get_item(
                    Key={"_id": keyname})["Item"]
            except:
                yield {}
                
    def gets(self, *args, **kwargs):
        """
        DES: upsert a list of data
        AUTHUR: Yeong
        INPUT: string, string, list of string
        USE CASE:
            # output = db.gets("test", "lambda", [ str(key) for key in range(100) ])
            where table name = test_lambda
        OUTPUT: list of dictionary
        EXCEPTION: 
            1. client object are None, not connect to database.
            2. input not 3 item
        LOG: Yeong, 2020-07-05, first version
            MH, 2020-07-06, do testing
        REMARKS: dynamodb every time query will expand the cache size so next time faster
        """
        
        # make sure the data have 3 element
        assert self._client
        dbname, colname, keynames = args
        
        # get and return data
        return list(self.__gets(dbname, colname, keynames))
    
        """# -- WIP -- by Yeong
        keynames = [ {"_id": k} for k in keynames ]
        # get and return data
        return self._client.batch_get_item(
            RequestItems={
                dbname+"_"+colname: {
                    "Keys": keynames
                }
            }
        )
        """
    
    def __dels(self, dbname, colname, keynames):
        
        # connection
        table = self._client.Table(dbname+"_"+colname)
        
        # batch delete
        with table.batch_writer() as batch:
            for keyname in keynames:
                try:
                    batch.delete_item(Key={"_id": keyname})
                    # yield success
                    yield True
                except:
                    # yield fail
                    yield False
                    
    def dels(self, *args, **kwargs):
        """
        DES: delete a list of data
        AUTHUR: Yeong
        INPUT: string, string, list of string
        USE CASE:
            output = db.dels("test","lambda", [ str(key) for key in range(100) ])
            where table name = test_lambda
        OUTPUT: list of boolean
        LOG: Yeong, 2020-07-05, first version
            MH, 2020-07-06, do testing
        REMARK: The delete is by random pick or hash pick, not by list sequence
        """
        
        # make sure the data have 3 element
        assert self._client
        dbname, colname, keynames = args
        
        # get and return data
        return list(self.__dels(dbname, colname, keynames))
    
    def pop(self, *args, **kwargs):
        """
        DES: find and delete a data
        AUTHUR: Yeong
        INPUT: string, string, string
        USE CASE:
            output = db.pop("test", "lambda", str(key))
            where table name = test_lambda
        OUTPUT: dictionary
        LOG: Yeong, 2020-07-05, first version
            MH, 2020-07-06, do testing
        """
        
        # make sure the data have 3 element
        assert self._client
        dbname, colname, keyname = args
        
        # pop and return data
        try:
            return self._client.Table(dbname+"_"+colname).delete_item(
                Key={"_id": keyname}, ReturnValues="ALL_OLD")["Attributes"]
        except:
            return {}
        
    def __pops(self, dbname, colname, keynames):
        
        # pop and yield data
        for keyname in keynames:
            try:
                yield self._client.Table(dbname+"_"+colname).delete_item(
                    Key={"_id": keyname}, ReturnValues="ALL_OLD")["Attributes"]
            except:
                yield {}
                
    def pops(self, *args, **kwargs):
        """
        DES: find and delete a list of data
        AUTHUR: Yeong
        INPUT: string, string, list of string
        USE CASE:
            output = db.pops("test", "lambda", [ str(key) for key in range(100) ])
            where table name = test_lambda
        OUTPUT: list of dictionary
        LOG: Yeong, 2020-07-05, first version
            MH, 2020-07-06, do testing
        REMARK: The delete is by random pick or hash pick, not by list sequence
        """
        
        # make sure the data have 3 element
        assert self._client
        dbname, colname, keynames = args
        
        # pop and return data
        return list(self.__pops(dbname, colname, keynames))
    
    def keys(self, *args, **kwargs):
        """
        DES: find a list of keys
        AUTHUR: Yeong
        INPUT: string, string, int
        USE CASE:
            output = db.dels("test","lambda", 10)
            where table name = test_lambda
        OUTPUT: list
        LOG: Yeong, 2020-07-05, first version
        LOG: MH, 2020-07-06, do testing
        REMARKS: get latest record first
                 not support nagative value
        """
        
        # make sure the data have 3 element
        assert self._client
        dbname, colname, size = args
        
        # get data
        data = [ i["_id"] for i in self._client.Table(dbname+"_"+colname).scan(Limit=int(size))["Items"] ]
        # data = self._client.Table(dbname+"_"+colname).scan(Limit=int(size))["Items"]
        
        # return data
        return data
        
    def find_one(self, *args, **kwargs):
        """
        DES: find latest item
        AUTHUR: mh
        INPUT: string, string
        USE CASE:
            output = db.find_one("test","lambda")
            where table name = test_lambda
        OUTPUT: dict
        LOG: MH, 2020-07-06, first version
        """
        
        # make sure the data have 3 element
        assert self._client
        dbname, colname = args
        
        # get data
        # return data
        try:
            return self._client.Table(dbname+"_"+colname).scan(Limit=int(1))["Items"][0]
        except:
            return {}
        
    def get_one(self, *args, **kwargs):
        """
        DES: find latest item
        AUTHUR: mh
        INPUT: string, string
        USE CASE:
            output = db.get_one("test","lambda")
            where table name = test_lambda
        OUTPUT: dict
        LOG: MH, 2020-07-06, first version
        """
        
        # make sure the data have 3 element
        assert self._client
        dbname, colname = args
        
        # get data
        # return data
        try:
            return self._client.Table(dbname+"_"+colname).scan(Limit=int(1))["Items"][0]
        except:
            return {}
        
def dynamodb_time_filter_query(client, dbname, colname, time, status):
    """
    DES: scan the whole table and filter "nexttime" and "status" attributes that fits the desired criteria
    AUTHUR: tessy
    INPUT: client, string, string, string, string
    USE CASE:
        output = dynamodb_time_filter_query(client,"test","scheduler","2020-07-09 10-08-28","live")
    OUTPUT: 
        if data: 
            list of dict
            eg.:[{"code": "IBC4", "starttime": "2019-01-01 04-12-00", "randomtime": Decimal("100000"), "timeinterval": Decimal("86400"), "status": "dead", "nexttime": "2019-01-01 04-12-00", "_id": "Germany.IBC4.NA", "exchange": "STU", "country": "Germany"}]
        if not data:
            empty list
            eg.: []
    LOG: tessy, 2020-07-09, first version
    """
    
    # checking
    assert client
    assert dbname
    assert colname
    assert time
    assert status
    
    # get filtered data
    data = client.Table(dbname+"_"+colname).scan(
            FilterExpression=Attr("nexttime").lt(time) & Attr("status").eq(status))["Items"]
    
    # return data
    return data
