"""
Des: 
    - this script will be triggered by API gateway
    - to manually refund those that failed to refund

Dependency: 
    - dynamodb -> { prod / stage / dev }_{ demo / live }_error_pending_attention_withdrawal
    - dynamodb -> { prod / stage / dev }_{ demo / live }_solved_pending_attention_withdrawal
    - environment variables of lambda:
        os.environ["error_table"] = "error_pending_attention_withdrawal"
        os.environ["solved_table"] = "solved_pending_attention_withdrawal"
        os.environ["lambda_name"] = "PentaipAwsFmDevelop-FmLam-Fm3cRoboOrderRefundFunct-BKB36QBE8B2V"
        os.environ["env"] = "dev" # dev / stage / prod
        
Sample Input:
    # case #1 sample input for API - event to read and/or refund per uuid
    {
        "_id": "15", # must have
        "account_type": "demo", # must have
        "action": "refund" # optional, depends on whether refund is required
    }
    
    # case #2 sample input for API - event to get all keys
    { 
        "account_type": "demo", # must have
        "row_count": "10" # optional, max count is 10
    }
    
    # sample api event passed into this script (not full version)
    event = {
                "httpMethod" : "POST",
                "body": json.dumps( event )
            }
    
Sample Output: 
    # case #1 - refund per uuid
    {
         'statusCode': 200, 
         'body': '"Successfully saved refunded item and deleted old item, _id: '19'"'
    }
    
    # case #2 - get all keys
    {
         'statusCode': 200, 
         'body': '{"keys": ["a8098c1a-f86e-11da-bd1a-00112444be1e", "48", "6", "5", "20", "47"]}'
    }
     
Author:
    Tessy
    
Log:
    Tessy   14 Oct 2020     First version of this script
    Tessy   19 Oct 2020     Added some checking and made some amendments to the logging
    
Status: 
    Alpha
"""

# import libs
import os
import json
import boto3
import datetime
try:
    from PentaipAwsFm.pluster.pluster_core.dynamodb_handler import DataHandler as Dynamodb_DataHandler
except:
    from pluster_core.dynamodb_handler import DataHandler as Dynamodb_DataHandler
  
# log messages
INFO_API_TRIGGERED = "[INFO] Triggered by api, File: 'Robo Order Manual Refund'"
INFO_REFUND_SUCCESS_LAMBDA = "[INFO] Refund success, File: 'Robo Order Manual Refund', _id:'{}', Lambda function: '{}'"
INFO_SAVE_SUCCESS_DYNAMODB = "[INFO] Saved success to dynamodb, File: 'Robo Order Manual Refund', _id: '{}', Table: '{}_{}'"
INFO_DELETE_SUCCESS_ITEM = "[INFO] Deleted success, File: 'Robo Order Manual Refund', _id: '{}', Table: '{}_{}'" 
INFO_GET_SUCCESS_KEYS = "[INFO] Get all keys success, File: 'Robo Order Manual Refund', Table: '{}_{}'"
INFO_REFUNDED = "[INFO] Refunded, File: 'Robo Order Manual Refund', _id: '{}', Refund at: '{}', Amount: '{}'"
INFO_NOT_YET_REFUND = "[INFO] Not yet refund, File: 'Robo Order Manual Refund', _id: '{}'"

DEBUG_START_LAMBDA = "[DEBUG] Start lambda, File: 'Robo Order Manual Refund'"
DEBUG_GET_DATA = "[DEBUG] Getting data, File: 'Robo Order Manual Refund', _id: '{}', Table: '{}_{}'"
DEBUG_TRIGGER_LAMBDA = "[DEBUG] Triggering refund lambda function, File: 'Robo Order Manual Refund', _id: '{}', Lambda function: '{}'"
DEBUG_SAVE_DYNAMODB = "[DEBUG] Saving to dynamodb, File: 'Robo Order Manual Refund', _id: '{}', Table: '{}_{}'"
DEBUG_DELETE_ITEM = "[DEBUG] Deleting item, File: 'Robo Order Manual Refund', _id: '{}', Table: '{}_{}'"
DEBUG_GET_ALL_KEYS = "[DEBUG] Getting all keys, File: 'Robo Order Manual Refund',  Table: '{}_{}'"

WARN_NO_EVENT = "[WARNING] No event data found, File: 'Robo Order Manual Refund'"
WARN_GET_EVENT = "[WARNING] Failed to get event, File: 'Robo Order Manual Refund', Due to: '{}'"
WARN_EVENT_NOT_DICT = "[WARNING] Event is not dictionary, File: 'Robo Order Manual Refund', Datatype: '{}'"
WARN_INVALID_ACCOUNT_TYPE = "[WARNING] Invalid 'account_type' value, File: 'Robo Order Manual Refund', Account Type: '{}'"
WARN_NO_RECORD = "[WARNING] No record found, File: 'Robo Order Manual Refund', _id: '{}'"
WARN_REFUND_FAIL_LAMBDA = "[WARNING] Refund fail, File: 'Robo Order Manual Refund', _id:'{}', Lambda function: '{}'"
WARN_SAVE_FAIL_DYNAMODB = "[WARNING] Saved fail to dynamodb, File: 'Robo Order Manual Refund', _id: '{}', Table: '{}_{}'"
WARN_DELETE_FAIL_ITEM = "[WARNING] Deleted fail, File: 'Robo Order Manual Refund', _id: '{}', Table: '{}_{}'" 
WARN_GET_FAIL_KEYS = "[WARNING] Get all keys fail, File: 'Robo Order Manual Refund', Table: '{}_{}'"

ERROR_TRIGGER_LAMBDA = "[ERROR] Unable to trigger lambda function, File: 'Robo Order Manual Refund', _id: '{}', Lambda function: '{}', Due to: '{}'"
ERROR_SAVE_DYNAMODB = "[ERROR] Unable to save to dynamodb, File: 'Robo Order Manual Refund', Due to '{}'"
ERROR_DELETE_ITEM = "[ERROR] Unable to delete item, File: 'Robo Order Manual Refund', _id: '{}', Due to: '{}'"

def __is_api_gw( event ):
    '''check whether event is triggered by api
    '''
    # checking 
    if event and event.get("httpMethod"):
        # return
        return True
        
    # return
    return False

def get_api_event( event ):
    '''get event from body of api
    '''
    # get data
    event = event.get( "body", {} )
    event = json.loads( event )
    
    # return
    return event

def get_error_data( event, dynamo_db ):
    '''get data from 'error' table
    '''
    # define variable
    dbname = os.environ["env"]
    colname = "{}_{}".format( event.get("account_type"), os.environ["error_table"] )
    
    # log 
    print( DEBUG_GET_DATA.format( event.get("_id"), dbname, colname ) )

    # get and return data
    return dynamo_db.get( dbname, colname, event.get("_id") )

def get_solved_data( data, dynamo_db ):
    '''get data from 'solved' table
    '''
    # define variable
    dbname = os.environ["env"]
    colname = "{}_{}".format( data.get("account_type"), os.environ["solved_table"] )
    
    # log 
    print( DEBUG_GET_DATA.format( data.get("_id"), dbname, colname ) )

    # get and return data
    return dynamo_db.get( dbname, colname, data.get("_id") )

def __is_refunded( data, dynamo_db ):
    '''check whether refund has been done
    '''
    #  get data
    data_ = get_solved_data( data, dynamo_db )
    
    # checking
    if data_:
        # log
        print( INFO_REFUNDED.format( data_.get("_id"), data_.get("refund_at"), data_.get("amount") ) )
        
        # return
        return True

    else:
        # log
        print( INFO_NOT_YET_REFUND.format( data.get("_id") ) )

        # return
        return False

def trigger_lambda( data ):
    '''trigger lambda
    '''
    # define variable
    lambda_function_name = os.environ["lambda_name"]
    
    # log
    print( DEBUG_TRIGGER_LAMBDA.format( data.get("_id"), lambda_function_name ) )
    
    # connect
    client = boto3.client( "lambda" )
    
    # trigger refund lambda
    res = client.invoke( 
            FunctionName = lambda_function_name,
            Payload = json.dumps( data ) )
    
    # get returned data
    payload = res['Payload'].read()
    returned_data = json.loads( payload.decode() )

    # check trigger status
    if returned_data.get("statusCode") == 200:
        # log
        print( INFO_REFUND_SUCCESS_LAMBDA.format( data.get("_id"), lambda_function_name ) )
        
        # return
        return True
    
    else: 
        # log
        print( WARN_REFUND_FAIL_LAMBDA.format( data.get("_id"), lambda_function_name ) )
        
        # return
        return False

def save2dynamodb( event, data, dynamo_db ):
    '''save to dynamodb
    '''
    # define variable
    dbname = os.environ["env"]
    colname = "{}_{}".format( event.get("account_type"), os.environ["solved_table"] )
    
    # log 
    print( DEBUG_SAVE_DYNAMODB.format( data.get("_id"), dbname, colname ) )

    # save
    res = dynamo_db.set( dbname, colname, data )
    
    # check saving status
    if res["ResponseMetadata"].get("HTTPStatusCode") == 200:
        # log
        print( INFO_SAVE_SUCCESS_DYNAMODB.format( data.get("_id"), dbname, colname ) )
        
        # return
        return True
    
    else: 
        # log
        print( WARN_SAVE_FAIL_DYNAMODB.format( data.get("_id"), dbname, colname ) )
        
        # return
        return False

def delete_item( event, data, dynamo_db ):
    '''delete item from dynamodb
    '''
    # define variable
    dbname = os.environ["env"]
    colname = "{}_{}".format( event.get("account_type"), os.environ["error_table"] )
    
    # log 
    print( DEBUG_DELETE_ITEM.format( data.get("_id"), dbname, colname ) )

    # delete
    res = dynamo_db.delete( dbname, colname, data.get("_id") )
    
    # log
    if res:
        print( INFO_DELETE_SUCCESS_ITEM.format( data.get("_id"), dbname, colname ) )
    
    else:
        print( WARN_DELETE_FAIL_ITEM.format( data.get("_id"), dbname, colname ) )
        
    # return
    return res
    
def fm_init_deco( func ):
    def inner( event, context ):
        '''initialize
        '''
        # log
        print( DEBUG_START_LAMBDA )
        
        # checking
        if not isinstance( event, dict ):
            # log
            print( WARN_EVENT_NOT_DICT.format( type( event ) ) )
            
            # return
            return {
                        "statusCode": 404,
                        "body": json.dumps( "Event must be dictionary" )
                    }
        
        # return function
        return func( event, context )
    
    # return
    return inner

def fm_security_checking_deco( func ):
    def inner( event, context ):
        '''checking the input for:
        - verification
        '''
        # checking
        if __is_api_gw( event ):
            # log 
            print( INFO_API_TRIGGERED )
            
            # get event
            try:
                event = get_api_event( event )
                
            except Exception as e:
                # log
                print( WARN_GET_EVENT.format( str(e) ) )
                
                # return
                return {
                            "statusCode": 404,
                            "body": json.dumps( "Failed to get api event, Due to: '{}'".format( str(e) ) )
                        }
        
        # return function
        return func( event, context )
    
    # return
    return inner

def fm_data_checking_deco( func ):
    def inner( event, context ):
        '''checking the input for:
        - invalid data
        '''
        # checking
        if not event:
            # log
            print( WARN_NO_EVENT )
            
            # return
            return {
                        "statusCode": 204,
                        "body" : json.dumps( "No event data found" )
                    }
    
        if not isinstance( event, dict ):
            # log
            print( WARN_EVENT_NOT_DICT.format( type( event ) ) )
            
            # return
            return {
                        "statusCode": 404,
                        "body" : json.dumps( "Event must be dictionary" )
                    }
        
        if event.get("account_type") and event.get("account_type") in [ "demo", "live" ]:
            # return function
            return func( event, context )
        
        else:
            # log
            print( WARN_INVALID_ACCOUNT_TYPE.format( event.get("account_type") ) )
            
            # return
            return {
                        "statusCode": 404,
                        "body" : json.dumps( "Invalid 'account_type' value, Account Type: '{}'".format( event.get("account_type") ) )
                    }
    
    # return
    return inner

@fm_init_deco
@fm_security_checking_deco
@fm_data_checking_deco
def main( event, context ):
    '''main
    sample input
    ------------
    # define event
    # event to read and/or refund per uuid
    event = {
                "_id": "10",
                "account_type": "demo",
                "action": "refund"
            }
    
    # sample api event (not full version)
    event = {
                "httpMethod" : "POST",
                "body": json.dumps( event )
            }
    '''
    # define variable
    save_res = False
    del_res = False
    lambda_res = False
    
    # connect
    dynamo_db = Dynamodb_DataHandler()
    dynamo_db.connect()
    
    # get data
    data = get_error_data( event, dynamo_db )
    
    # checking
    if not data:
        # log
        print( WARN_NO_RECORD.format( event.get("_id") ) )
        
        # close connection
        dynamo_db.close()
            
        # return
        return {
                    "statusCode": 404,
                    "body": json.dumps( "Invalid '_id', _id: '{}'".format( event.get("_id") ) )
                }

    # checking
    if event.get("action") and event.get("action") == "refund":
        if __is_refunded( data, dynamo_db ):
            
            # delete item
            try:
                del_res = delete_item( event, data, dynamo_db )
        
            except Exception as e:
                # log
                print( ERROR_DELETE_ITEM.format( data.get("_id"), str(e) ) )
        
            # close connection
            dynamo_db.close()
            
            # return
            if del_res:
                return {
                            "statusCode": 200,
                            "body": json.dumps( "Refund has been done previously, successfully deleted old item, _id: '{}'".format( data.get("_id") ) )
                        }
                
            else:
                return {
                            "statusCode": 404,
                            "body": json.dumps( "Refund has been done previously, fail to delete old item, _id: '{}'".format( data.get("_id") ) )
                        }
        
        # trigger refund lambda
        try:
            lambda_res = trigger_lambda( data )
            
        except Exception as e:
            # log 
            print( ERROR_TRIGGER_LAMBDA.format( data.get("_id"), os.environ["lambda_name"], str(e) ) )
            
            # close connection
            dynamo_db.close()
            
            # return
            return {
                        "statusCode": 404,
                        "body": json.dumps( ( "Unable to trigger lambda function, _id: '{}', Lambda function: '{}', Due to: '{}'".format( data.get("_id"), os.environ["lambda_name"], str(e) ) ) )
                    }
        
        # checking
        if lambda_res:
            # add/edit data
            data["status"] = "refunded"
            data["refund_at"] = datetime.datetime.utcnow().strftime( "%Y-%m-%d %H:%M:%S.%f" )
            
            # save to dynamodb
            try:
                save_res = save2dynamodb( event, data, dynamo_db )
            
            except Exception as e:
                # log
                print( ERROR_SAVE_DYNAMODB.format( str(e) ) )
            
            if save_res:
                # delete item
                try:
                    del_res = delete_item( event, data, dynamo_db )
            
                except Exception as e:
                    # log
                    print( ERROR_DELETE_ITEM.format( data.get("_id"), str(e) ) )
            
            # close connection
            dynamo_db.close()
            
            # return
            if del_res:
                return {
                            "statusCode": 200,
                            "body": json.dumps( "Successfully saved refunded item and deleted old item, _id: '{}'".format( data.get("_id") ) )
                        }
                
            else:
                return {
                            "statusCode": 404,
                            "body": json.dumps( "Fail to save refunded item and delete old item, _id: '{}'".format( data.get("_id") ) )
                        }
        
        else:
            # close connection
            dynamo_db.close()
            
            # return
            return {
                        "statusCode": 404,
                        "body": json.dumps( ( "Fail to refund, _id: '{}'".format( data.get("_id") ) ) )
                    }
        
    else:
        # close connection
        dynamo_db.close()
        
        # return
        return {
                    "statusCode": 200,
                    "body": json.dumps( data )
                }
        
@fm_init_deco
@fm_security_checking_deco
@fm_data_checking_deco
def get_all_keys( event, context ):
    '''get all keys from dynamodb table
    sample input
    ------------
    # event to get all keys
    event = { 
                "account_type": "demo",
                "row_count": "10"
            }
    '''
    # define variable
    dbname = os.environ["env"]
    colname = "{}_{}".format( event.get("account_type"), os.environ["error_table"] )
    row_count = float( event.get("row_count") ) if event.get("row_count") and float( event.get("row_count") ) <= 10 else 10
    
    # log 
    print( DEBUG_GET_ALL_KEYS.format( dbname, colname ) )
    
    # connect
    dynamo_db = Dynamodb_DataHandler()
    dynamo_db.connect()
    
    # get data
    keys = dynamo_db.keys( dbname, colname, row_count )
    
    # close connection
    dynamo_db.close()

    # return
    if keys:
        # log
        print( INFO_GET_SUCCESS_KEYS.format( dbname, colname ) )
        
        return  {
                    "statusCode": 200,
                    "body": json.dumps( { "keys": keys } )
                }
    
    else:
        # log
        print( WARN_GET_FAIL_KEYS.format( dbname, colname ) )
        
        return {
                    "statusCode": 404,
                    "body": json.dumps( "Unable to get all keys, Table: '{}_{}".format( dbname, colname ) )
                }
    
    
# # test run 
# # TODO: comment this before push to aws
# if __name__ == "__main__":
#     # simulate aws environment
#     os.environ["env"] = "dev"
#     os.environ["error_table"] = "error_pending_attention_withdrawal"
#     os.environ["solved_table"] = "solved_pending_attention_withdrawal"
#     os.environ["lambda_name"] = "PentaipAwsFmDevelop-FmLam-Fm3cRoboOrderRefundFunct-BKB36QBE8B2V"
        
#     # define event
#     # event to read and/or refund per uuid
#     event = {
#                 "_id": "20",
#                 "account_type": "demo",
#                 "action": "refund"
#             }
    
#     # event to get all keys
#     event = { 
#                 "account_type": "demo",
#                 "row_count": "10"
#             }
    
#     # sample api event (not full version)
#     event = {
#                 "httpMethod" : "POST",
#                 "body": json.dumps( event )
#             }
    
#     # main_res = main( event, "" )
#     keys_res = get_all_keys( event, "" )
    