#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  7 11:45:21 2020

Des: this script is to use threading to stress test fm1a

@author: tessy
"""

# import libs
import uuid
import json
import random
import threading
try:
    import requests
except:
    from botocore.vendored import requests
try:
    from PentaipAwsData.pluster.pluster_core.pata_framework2 import Patafw as Patafw
except:
    from pluster_core.pata_framework2 import Patafw as Patafw

# define variables
url = "https://wa9134zdvl.execute-api.us-east-2.amazonaws.com/fmapi/fm/request-receive"
success_list = []
fail_list = []
conn_error_list = []

def generate_sample_data( range_ = 0 ):
    '''generate random sample data for fm1a
    ''' 
    # define variable
    tmp_list = []
    action_list = [ "deposit" ]
    currency_list = [ "HUF", "INR", "TWD", "GBP", "PLN", "JPY", "SEK", "THB", "EUR", "NOK", "MYR", "CHF", "DKK", "CNY", "IDR", "SAR", "CAD", "ISK", "PKR", "PHP", "TRY", "KRW", "MXN", "ILS", "ZAR", "BRL", "VND", "GBX", "USD", "CLP", "HKD", "SGD" ]
    account_type_list = [ "demo" ]
    user_id_list = [ 
                        "001", "002", "003", "004", "005", "006", "007", "008", "009", "010", "011", "012", "013", "014", "015", \
                        "016", "017", "018", "019", "020", "021", "022", "023", "024", "025", "026", "027", "028", "029", "030"
                    ]
    
    # loop and generate sample data
    for count in range( range_ ):
        #get currency
        currency = random.choice( currency_list )
        
        # set data
        event = {
                    "user_id": random.choice( user_id_list ),
                    "robo_type": "Robo_{}".format( currency ),
                    "amount": str( random.uniform( 0, 200) ),
                    "action": random.choice( action_list ),
                    "currency": currency,
                    "account_type": random.choice( account_type_list )
                }
        
        # append to list
        tmp_list.append( event )
        
    # return
    return tmp_list

def multiply_event( event_list, multiple ):
    '''multiply more events
    '''
    # multiply events
    event = event_list
    event *= multiple
    
    for i in range( 0, len(event) ):
        event[i] = dict( event[i] )
        event[i]['_id'] = str( i ) # str( uuid.uuid4() )
        
    # return
    return event
    
def get_theory_value( multiple_event ):
    '''calculate theory value
    '''
    # define variable
    tmp_dict = {}
    
    # get theory value
    for event in multiple_event:
        # sum up robo deposit amount
        try:
            tmp_dict[ event.get("robo_type") ] = str( float( tmp_dict.get( event.get("robo_type") ) ) + float( event.get("amount") ) )
        except:
            tmp_dict[ event.get("robo_type") ] = str( float( "0" ) + float( event.get("amount") ) )
            
        # define _id for user wallet
        _id = event.get("user_id") + "_" + event.get("currency")
        
        # sum up user deducted amount
        try:
            tmp_dict[ _id ] = str( float( tmp_dict.get( _id ) ) + float( event.get("amount") ) )
        except:
            tmp_dict[ _id ] = str( float( "0" ) + float( event.get("amount") ) )
            
    # return
    return tmp_dict
    
def start_threading( multiple_event ):
    '''threading ( not using, replaced by pata framework )
    ''' 
    # define variable
    threads = list()
    
    # threading
    for event in multiple_event:
        x = threading.Thread( target=post_request, args=( event, ) )
        threads.append( x )
        x.start()

    for index, thread in enumerate( threads ):
        thread.join()
     
    # return
    return 

def post_request( event ):
    '''post request
    '''
    # post request
    try:
        x = requests.post( url, data = json.dumps( event ) )
    
    except ConnectionError as e:
        conn_error_list.append( { event.get("_id") : e } )
    
    # log
    print( "Requested '{}' > '{}'".format( event.get("_id"), x.status_code ) )
    # checking status code 
    if x.status_code == 200:
        success_list.append( { x.status_code : event.get("_id") } )
        
    else:
        fail_list.append( { x.status_code : event.get("_id") } )
        

# # test run 
# if __name__ == "__main__":
#     # define variable
#     range_ = 300
#     multiple = 500
#     worker = 200
    
#     # generate sample data
#     event_list = generate_sample_data( range_ )
  
#     # multiply event
#     event = multiply_event( event_list, multiple )
    
#     # theory value
#     theory_res = get_theory_value( event )

#     # threading
#     f = Patafw()
#     f.trace()
#     f.id().thread( post_request ).input( event ).worker( worker, 1 )
#     f.run()