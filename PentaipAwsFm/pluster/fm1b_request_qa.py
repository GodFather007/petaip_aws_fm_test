"""
Des: 
    - this script will be triggered by sqs event source mapping
    - this script will trigger state machine depending on the "action"
    - this script will check the "action", whether its "deposit" or "withdraw", and trigger the respective state machine

Dependency: 
    - sqs -> { prod / stage / dev }_{ demo / live }_fm_submit_req_queue 
    - sf -> { prod / stage / dev }_fm_request_deposit_state_machine
    - environment variables of lambda:
        os.environ["env"] = "dev" # dev / stage / prod
        os.environ["sf_deposit_name"] = "fm_request_deposit_state_machine"
        
Sample Input:
    # sample msg passed from sqs
    {
        'Records': [{
            'messageId': '5050fd45-327b-4923-ac48-d37388b63549', 
            'receiptHandle': 'AQEBvTE3iiVC ... bLvN7R8JU1FK8AnS97xEmo=', 
            'body': '{
                "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e",
                "user_id": "001",
                "robo_type": "Robo_USD",
                "amount": "100",
                "action": "deposit",
                "currency": "USD",
                "account_type": "demo",
                "received_at": "2020-10-19 08:59:10.881638",
                "status": "received"
            }', 
            'attributes': {'ApproximateReceiveCount': '1', 'SentTimestamp': '1602210795408', 'SenderId': 'AROAYOWNWXFAO6AW7MZIM:PentaipAwsFmDevelop-FmLam-Fm1aRequestReceiveFunction-1U34J434LIJN9', 'ApproximateFirstReceiveTimestamp': '1602210795409'}, 
            'messageAttributes': {}, 
            'md5OfBody': '850041fff21e3c8e1d86a8b940b63570', 
            'eventSource': 'aws:sqs', 'eventSourceARN': 'arn:aws:sqs:us-east-2:581325732160:dev_demo_fm_submit_req_queue', 'awsRegion': 'us-east-2'
            }]
        }

Sample Output: 
    {'statusCode': 200, 'body': '"Step function triggered success, _id: 'a8098c1a-f86e-11da-bd1a-00112444be1e', Step function: 'dev_fm_request_deposit_state_machine'"'}
    
Sample Data ( data passed to step function ):
    {
         '_id': 'a8098c1a-f86e-11da-bd1a-00112444be1e', 
         'user_id': '001', 
         'robo_type': 'Robo_USD', 
         'amount': '100', 
         'action': 'deposit', 
         'currency': 'USD', 
         'account_type': 'demo', 
         'received_at': '2020-10-19 08:59:10.881638', 
         'status': 'received'
     }
    
Author:
    Tessy
    
Log:
    Tessy   13 Oct 2020     First version of this script
    Tessy   19 Oct 2020     Added some checking and made some amendments to the logging
    
Status: 
    Alpha
"""

# import libs
import os
import json
import boto3
  
# log messages
INFO_SQS_TRIGGERED = "[INFO] Triggered by sqs, File: 'Request Qa'"
INFO_TRIGGER_SUCCESS_SF = "[INFO] Trigger success step function, File: 'Request Qa', _id:'{}', Step function: '{}'"

DEBUG_START_LAMBDA = "[DEBUG] Start lambda, File: 'Request Qa'"
DEBUG_START_LAMBDA_ID = "[DEBUG] Start lambda by ID, File: 'Request Qa', _id: '{}'"
DEBUG_TRIGGER_SF = "[DEBUG] Triggering step function, File: 'Request Qa', _id: '{}', Step function: '{}'"

WARN_INVALID_ACTION = "[WARNING] Invalid 'action' value, File: 'Request Qa', Action: '{}'"
WARN_TRIGGER_FAIL_SF = "[WARNING] Trigger fail step function, File: 'Request Qa', _id: '{}', Step function: '{}'"

ERROR_TRIGGER_SF = "[ERROR] Unable to trigger step function, File: 'Request Qa', _id: '{}', Step function: '{}', Due to: '{}'"

def __is_sqs( event ):
    '''check whether event is triggered by sqs
    '''
    # checking
    if event.get("Records") and event["Records"][0].get("eventSource") == "aws:sqs":
        # return
        return True
    
    # return
    return False

def get_sqs_event( event ):
    '''get event from body of sqs
    '''
    # get data
    event = event["Records"][0].get( "body", {} )
    event = json.loads( event )
    
    # return
    return event
       
def trigger_step_function( event, step_function_name ):
    '''trigger step function
    '''
    # log
    print( DEBUG_TRIGGER_SF.format( event.get("_id"), step_function_name ) )
    
   # connect
    client = boto3.client( "stepfunctions" )
    
    # trigger
    res = client.start_execution(
        stateMachineArn = "arn:aws:states:us-east-2:581325732160:stateMachine:{}".format( step_function_name ),
        input= json.dumps( event )
    )
        
    # check trigger status
    if res.get( "ResponseMetadata", {} ).get("HTTPStatusCode") == 200:
        # log
        print( INFO_TRIGGER_SUCCESS_SF.format( event.get("_id"), step_function_name ) )
        
        # return
        return True
    
    else: 
        # log
        print( WARN_TRIGGER_FAIL_SF.format( event.get("_id"), step_function_name ) )
        
        # return
        return False

def fm_init_deco( func ):
    def inner( event, context ):
        '''initialize
        '''
        # log
        print( DEBUG_START_LAMBDA )
      
        # return function
        return func( event, context )
    
    # return
    return inner

def fm_security_checking_deco( func ):
    def inner( event, context ):
        '''checking the input for:
        - verification
        '''
        # checking
        if __is_sqs( event ):
            # log 
            print( INFO_SQS_TRIGGERED )
            
            # get event
            event = get_sqs_event( event )
                
        # return function
        return func( event, context )
    
    # return
    return inner

def fm_data_checking_deco( func ):
    def inner( event, context ):
        '''checking the input for:
        - invalid data
        '''    
        # log
        print( DEBUG_START_LAMBDA_ID.format( event.get("_id") ) )
           
        # return function
        return func(event, context)
    
    # return
    return inner

@fm_init_deco
@fm_security_checking_deco
@fm_data_checking_deco
def main( event, context ):
    '''main
    sample input
    ------------
    # define event
    event = {
                "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e",
                "user_id": "001",
                "robo_type": "Robo_USD",
                "amount": "100",
                "action": "deposit",
                "currency": "USD",
                "account_type": "demo",
                "received_at": "2020-10-19 08:59:10.881638",
                "status": "received"
            }
    
    # sample sqs event with batch size 1 (not full version)
    event = {
                'Records': [{
                    'messageId': '5050fd45-327b-4923-ac48-d37388b63549', 
                    'receiptHandle': 'AQEBvTE3iiVC ... bLvN7R8JU1FK8AnS97xEmo=', 
                    'body': json.dumps( event ), 
                    'attributes': {'ApproximateReceiveCount': '1', 'SentTimestamp': '1602210795408', 'SenderId': 'AROAYOWNWXFAO6AW7MZIM:PentaipAwsFmDevelop-FmLam-Fm1aRequestReceiveFunction-1U34J434LIJN9', 'ApproximateFirstReceiveTimestamp': '1602210795409'}, 
                    'messageAttributes': {}, 
                    'md5OfBody': '850041fff21e3c8e1d86a8b940b63570', 
                    'eventSource': 'aws:sqs', 'eventSourceARN': 'arn:aws:sqs:us-east-2:581325732160:dev_demo_fm_submit_req_queue', 'awsRegion': 'us-east-2'
                    }]
                }
    '''
    # define variable
    step_function_name = None
    sf_res = False

    if event.get("action") == "deposit":
       step_function_name = "{}_{}".format( os.environ["env"], os.environ["sf_deposit_name"] )
       
    elif event.get("action") == "withdraw":
        step_function_name = "TessyTestDateTime" # later on change to : "{}_{}".format( os.environ["env"], os.environ["sf_withdrawal_name"] )
        
    else:
        # log
        print( WARN_INVALID_ACTION.format( event.get("action") ) )
        
        # return
        return {
                    "statusCode": 404,
                    "body": json.dumps( "Invalid 'action' value, Action: '{}'".format( event.get("action") ) )
               }
    
    # trigger step function
    try:
        sf_res = trigger_step_function( event, step_function_name )
    
    except Exception as e:
        # log
        print( ERROR_TRIGGER_SF.format( event.get("_id"), step_function_name, str(e) ) )
        
        # return
        return {
                    "statusCode": 404,
                    "body": json.dumps( "Unable to trigger step function, _id: '{}', Step function: '{}', Due to: '{}'".format( event.get("_id"), step_function_name, str(e) ) )
               }
    
    # return
    if sf_res:
        return {
                    "statusCode": 200,
                    "body": json.dumps( "Step function triggered success, _id: '{}', Step function: '{}'".format( event.get("_id"), step_function_name ) )
                }
    else:
        return {
                    "statusCode": 404,
                    "body": json.dumps( "Step function triggered fail, _id: '{}', Step function: '{}'".format( event.get("_id"), step_function_name ) )
                }


# # test run 
# # TODO: comment this before push to aws
# if __name__ == "__main__":
#     # simulate aws environment
#     os.environ["env"] = "dev"
#     os.environ["sf_deposit_name"] = "fm_request_deposit_state_machine"
        
#     # define variable
#     event = {
#                 "_id": "a8098c1a-f86e-11da-bd1a-00112444be1e",
#                 "user_id": "001",
#                 "robo_type": "Robo_USD",
#                 "amount": "100",
#                 "action": "deposit",
#                 "currency": "USD",
#                 "account_type": "demo",
#                 "received_at": "2020-10-19 08:59:10.881638",
#                 "status": "received"
#             }
    
#     # sample sqs event with batch size 1 (not full version)
#     event = {
#                 'Records': [{
#                     'messageId': '5050fd45-327b-4923-ac48-d37388b63549', 
#                     'receiptHandle': 'AQEBvTE3iiVC ... bLvN7R8JU1FK8AnS97xEmo=', 
#                     'body': json.dumps( event ), 
#                     'attributes': {'ApproximateReceiveCount': '1', 'SentTimestamp': '1602210795408', 'SenderId': 'AROAYOWNWXFAO6AW7MZIM:PentaipAwsFmDevelop-FmLam-Fm1aRequestReceiveFunction-1U34J434LIJN9', 'ApproximateFirstReceiveTimestamp': '1602210795409'}, 
#                     'messageAttributes': {}, 
#                     'md5OfBody': '850041fff21e3c8e1d86a8b940b63570', 
#                     'eventSource': 'aws:sqs', 'eventSourceARN': 'arn:aws:sqs:us-east-2:581325732160:dev_demo_fm_submit_req_queue', 'awsRegion': 'us-east-2'
#                     }]
#                 }
    
#     # # sample sqs event with batch size 2 (not full version)
#     # event = {
#     #             'Records': [{
#     #                 'messageId': '5050fd45-327b-4923-ac48-d37388b63549', 
#     #                 'receiptHandle': 'AQEBvTE3iiVC ... bLvN7R8JU1FK8AnS97xEmo=', 
#     #                 'body': json.dumps( event ), 
#     #                 'attributes': {'ApproximateReceiveCount': '1', 'SentTimestamp': '1602210795408', 'SenderId': 'AROAYOWNWXFAO6AW7MZIM:PentaipAwsFmDevelop-FmLam-Fm1aRequestReceiveFunction-1U34J434LIJN9', 'ApproximateFirstReceiveTimestamp': '1602210795409'}, 
#     #                 'messageAttributes': {}, 
#     #                 'md5OfBody': '850041fff21e3c8e1d86a8b940b63570', 
#     #                 'eventSource': 'aws:sqs', 'eventSourceARN': 'arn:aws:sqs:us-east-2:581325732160:dev_demo_fm_submit_req_queue', 'awsRegion': 'us-east-2'
#     #                 },{
#     #                 'messageId': '5050fd45-327b-4923-ac48-d37388b63549', 
#     #                 'receiptHandle': 'AQEBvTE3iiVC ... bLvN7R8JU1FK8AnS97xEmo=', 
#     #                 'body': json.dumps( event ), 
#     #                 'attributes': {'ApproximateReceiveCount': '1', 'SentTimestamp': '1602210795408', 'SenderId': 'AROAYOWNWXFAO6AW7MZIM:PentaipAwsFmDevelop-FmLam-Fm1aRequestReceiveFunction-1U34J434LIJN9', 'ApproximateFirstReceiveTimestamp': '1602210795409'}, 
#     #                 'messageAttributes': {}, 
#     #                 'md5OfBody': '850041fff21e3c8e1d86a8b940b63570', 
#     #                 'eventSource': 'aws:sqs', 'eventSourceARN': 'arn:aws:sqs:us-east-2:581325732160:dev_demo_fm_submit_req_queue', 'awsRegion': 'us-east-2'
#     #                 }]
#     #             }
    
#     res = main( event, "" )